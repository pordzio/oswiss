/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.core;

import static org.junit.Assert.*;
import org.junit.Test;
import pl.katowice.poldrag.swiss.core.KValues.Entry;

/**
 *
 * @author kkopec
 */
public class KValuesTest {
    
    public KValuesTest() {
    }

    /**
     * Test of put method, of class KValues.
     */
    @Test
    public void testPut() {
        System.out.println("put");
        KValues instance = new KValues();
        assertEquals(0, instance.size());
        
        Entry entry1 = new Entry(1, 4);
        Entry entry2 = new Entry(2, 5);
        Entry entry3 = new Entry(3, 6);
        
        Integer result = instance.put(entry1);
        assertEquals(1, instance.size());
        assertEquals(null, result);

        result = instance.put(entry2);
        assertEquals(2, instance.size());
        assertEquals(null, result);
        
        result = instance.put(entry3);
        assertEquals(3, instance.size());
        assertEquals(null, result);
        
        result = instance.put(entry1);
        assertEquals(3, instance.size());
        assertEquals(entry1.getkValue(), result);
    }

    /**
     * Test of serialize method, of class KValues.
     */
    @Test
    public void testSerialize() {
        System.out.println("serialize");
        KValues instance = new KValues();
        assertEquals("", instance.serialize());
        
        instance.put(1, 2);
        assertEquals("(1,2)", instance.serialize());

        instance.put(5, 6);
        assertEquals("(1,2)(5,6)", instance.serialize());

        instance.put(3, 4);
        assertEquals("(1,2)(3,4)(5,6)", instance.serialize());

        instance.clear();
        assertEquals("", instance.serialize());
    }

    /**
     * Test of unserialize method, of class KValues.
     */
    @Test
    public void testUnserialize() {
        System.out.println("unserialize");

        KValues instance = KValues.unserialize("(1,2)(3,4)(5,6)");
        assertEquals(3, instance.size());
        assertEquals(4, instance.get(3).intValue());
        assertEquals(6, instance.get(5).intValue());
        assertEquals(null, instance.get(6));
        
        instance = KValues.unserialize("");
        assertEquals(0, instance.size());
        
        instance = KValues.unserialize("Hey ho!");
        assertEquals(0, instance.size());
    }

    /**
     * Test of getKValue method, of class KValues.
     */
    @Test
    public void testGetKValue() {
        System.out.println("getKValue");

        KValues instance = KValues.unserialize("(1,2)(3,4)(5,6)");
        assertEquals(1, instance.getKValue(-100));
        assertEquals(1, instance.getKValue(0));
        assertEquals(2, instance.getKValue(1));
        assertEquals(2, instance.getKValue(2));
        assertEquals(4, instance.getKValue(3));
        assertEquals(4, instance.getKValue(4));
        assertEquals(6, instance.getKValue(5));
        assertEquals(6, instance.getKValue(6));
        assertEquals(6, instance.getKValue(10));
        
        instance = KValues.unserialize("");
        assertEquals(1, instance.getKValue(-100));
        assertEquals(1, instance.getKValue(0));
        assertEquals(1, instance.getKValue(1));
        assertEquals(1, instance.getKValue(2));
        assertEquals(1, instance.getKValue(3));
        assertEquals(1, instance.getKValue(4));
        assertEquals(1, instance.getKValue(5));
        assertEquals(1, instance.getKValue(6));
        assertEquals(1, instance.getKValue(10));
    }

}
