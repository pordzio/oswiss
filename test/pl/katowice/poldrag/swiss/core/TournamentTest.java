/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.core;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.junit.*;
import pl.katowice.poldrag.swiss.exceptions.SwissException;


/**
 *
 * @author kkopec
 */
public class TournamentTest {
    
    private boolean debug = false;
    
    public TournamentTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    private boolean checkTournament(Swiss t)
    {
        int NOT_PLAYED = -1;
        
        if (t == null)
            return false;
        
        List<Player> players = t.getPlayerList();
        List<Round> rounds = new ArrayList<Round>();
        for (int i=0; i<t.getRoundsCount(); i++)
            rounds.add(t.getRound(i));
        
        // tworzenie matrixa i sprawdzanie, czy gracze nie grali
        // ze sobą po kilka razy
        int [][] matrix = new int[players.size()+1][players.size()+1];
            
        for (int j=0; j<players.size()+1; j++)
        for (int i=0; i<players.size()+1; i++)
            matrix[i][j] = NOT_PLAYED;       
            
        int id = 0;
        for (Player pl : players)
            pl.setReserved1(id++);
        Player.playerPause.setReserved1(id);

        for (Round r : rounds)
        for (Pair p : r.getPairs()) {
            
            if (r.getPairs().size() != (players.size() + 1)/2) {
                System.out.println("Not all players has ben paired.");
                return false;
            }
            
            if (matrix[p.getPlayer1().getReserved1()][p.getPlayer2().getReserved1()] != NOT_PLAYED
             || matrix[p.getPlayer2().getReserved1()][p.getPlayer1().getReserved1()] != NOT_PLAYED) {
                System.out.println("Players played each others several times!");
                return false;
            }
            
            matrix[p.getPlayer1().getReserved1()][p.getPlayer2().getReserved1()] = p.getPoints1();
            matrix[p.getPlayer2().getReserved1()][p.getPlayer1().getReserved1()] = p.getPoints2();
        }
        
        return true;        
    }
    
    private void fillResults(Tournament t)
    {
        Round r = t.currentRound();
        if (r == null)
            return;

        if (debug)
            System.out.println("Round: " + t.getRoundsCount());
        
        for (Pair p : r.getPairs()) {  
            p.setPoints(3, 0);
            if (debug) System.out.println(p);
        }

        if (debug) System.out.println("---");
    }
    
    /**
     * Test of startNewRound method, of class Tournament.
     */
    @Test(timeout=10000)
    public void testStartNewRound() throws Exception {
        Player.playerMiss = new Player("MISS", Player.PLAYER_ID_MISS);
        Player.playerPause = new Player("PAUSE", Player.PLAYER_ID_PAUSE);
        System.out.println("startNewRound");
        Swiss t = new Swiss("test");
        t.addPlayer(new Player("AAA"));
        t.addPlayer(new Player("BBB"));
        t.addPlayer(new Player("CCC"));
        t.addPlayer(new Player("DDD"));
        t.addPlayer(new Player("EEE"));
        
        try {
            t.startNewRound();
            fillResults(t);
            assertTrue(checkTournament(t));
            t.startNewRound();
            fillResults(t);
            assertTrue(checkTournament(t));
            t.startNewRound();
            fillResults(t);
            assertTrue(checkTournament(t));
            t.startNewRound();
            fillResults(t);
            assertTrue(checkTournament(t));
        } catch (SwissException se) {
            System.out.print("catch: pairing not possible");
        }
    }
    
    /*
    @Test
    public void testCurrentRound() {
        System.out.println("currentRound");
        int expResult = 0;
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetRound() {
        System.out.println("getRound");
        int number = 0;
        fail("The test case is a prototype.");
    }   
    
    @Test
    public void testDeleteLastRound() throws Exception {
        System.out.println("deleteLastRound");
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetPlayerList() {
        System.out.println("getPlayerList");
        fail("The test case is a prototype.");
    }

    @Test
    public void testAddPlayer() throws Exception {
        System.out.println("addPlayer");
        fail("The test case is a prototype.");
    }

    @Test
    public void testDelPlayer_Player() throws Exception {
        System.out.println("delPlayer");
        fail("The test case is a prototype.");
    }

    @Test
    public void testDelPlayer_int() throws Exception {
        System.out.println("delPlayer");
        int playerIndex = 0;
        fail("The test case is a prototype.");
    }
    */
}
