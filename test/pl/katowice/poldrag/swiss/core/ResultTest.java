/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.core;

import static org.junit.Assert.assertTrue;
import org.junit.*;

/**
 *
 * @author kkopec
 */
public class ResultTest {

    public ResultTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    /**
     * Test of revert method, of class Result.
     */
    @Test
    public void testRevert() {
        System.out.println("revert");

        Result res;
        Result rev;

        res = new Result(2, 3);
        rev = new Result(res);
        rev.revert();
        assertTrue(res.getPoints1()==rev.getPoints2() && res.getPoints2()==rev.getPoints1());
    }

    /**
     * Test of toString method, of class Result.
     */
    @Test
    public void testToString() {
        System.out.println("toString");

        Result res = new Result(2, 3);
        assertTrue(res.toString().equals("(2 - 3)"));

        res = new Result(123, 000);
        assertTrue(res.toString().equals("(123 - 0)"));

        res = new Result(0, 0);
        assertTrue(res.toString().equals("(0 - 0)"));
    }

    /**
     * Test of parseRes method, of class Result.
     */
    @Test
    public void testParseRes() {
        System.out.println("parseRes");
        String s;
        Result res;

        s = "(1 - 3)";
        res = Result.parseRes(s);
        assertTrue(res.getPoints1()==1 && res.getPoints2()==3);

        s = "   (   132 - 003   )   ";
        res = Result.parseRes(s);
        assertTrue(res.getPoints1()==132 && res.getPoints2()==3);

        s = "( 5 2 3 -22 1 111) ";
        res = Result.parseRes(s);
        assertTrue(res.getPoints1()==523 && res.getPoints2()==221111);

        s = "(0.123 - 231)";
        res = Result.parseRes(s);
        assertTrue(res == null);

        s = "( - 0 - 3 )";
        res = Result.parseRes(s);
        assertTrue(res == null);

        s = null;
        res = Result.parseRes(s);
        assertTrue(res == null);

        s = "";
        res = Result.parseRes(s);
        assertTrue(res == null);

        s = "()";
        res = Result.parseRes(s);
        assertTrue(res == null);
    }
}
