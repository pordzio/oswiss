package pl.katowice.poldrag.swiss.exceptions;


public class SwissTopRoundMissing extends SwissException { 

    public SwissTopRoundMissing(String message) {
        super(message);
    }
    
    public SwissTopRoundMissing() {
        super();
    }
    
}
