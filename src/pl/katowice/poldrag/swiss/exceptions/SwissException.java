package pl.katowice.poldrag.swiss.exceptions;

import java.lang.Exception;



public class SwissException extends Exception { 

    public SwissException(String message) {
        super(message);
    }
    
    public SwissException() {
        super();
    }
    
}
