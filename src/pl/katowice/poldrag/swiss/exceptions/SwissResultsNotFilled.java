package pl.katowice.poldrag.swiss.exceptions;

import java.lang.Exception;



public class SwissResultsNotFilled extends SwissException { 

    public SwissResultsNotFilled(String message) {
        super(message);
    }
    
    public SwissResultsNotFilled() {
        super();
    }
    
}
