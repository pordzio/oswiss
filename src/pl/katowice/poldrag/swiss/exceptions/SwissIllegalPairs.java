package pl.katowice.poldrag.swiss.exceptions;

import java.lang.Exception;



public class SwissIllegalPairs extends SwissException { 

    public SwissIllegalPairs(String message) {
        super(message);
    }
    
    public SwissIllegalPairs() {
        super();
    }
    
}
