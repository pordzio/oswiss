package pl.katowice.poldrag.swiss.exceptions;

import java.lang.Exception;



public class SwissPairingTopNotPossible extends SwissException { 

    public SwissPairingTopNotPossible(String message) {
        super(message);
    }
    
    public SwissPairingTopNotPossible() {
        super();
    }
    
}
