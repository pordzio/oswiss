package pl.katowice.poldrag.swiss.exceptions;


public class SwissNoRounds extends SwissException { 

    public SwissNoRounds(String message) {
        super(message);
    }
    
    public SwissNoRounds() {
        super();
    }
    
}
