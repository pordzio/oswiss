package pl.katowice.poldrag.swiss.exceptions;

import java.lang.Exception;



public class SwissPlayerNameCollision extends SwissException { 

    public SwissPlayerNameCollision(String message) {
        super(message);
    }
    
    public SwissPlayerNameCollision() {
        super();
    }
    
}
