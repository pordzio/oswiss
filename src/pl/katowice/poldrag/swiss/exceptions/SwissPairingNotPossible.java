package pl.katowice.poldrag.swiss.exceptions;

import java.lang.Exception;



public class SwissPairingNotPossible extends SwissException { 

    public SwissPairingNotPossible(String message) {
        super(message);
    }
    
    public SwissPairingNotPossible() {
        super();
    }
    
}
