package pl.katowice.poldrag.swiss.exceptions;

import java.lang.Exception;



public class SwissEmptyPlayerList extends SwissException { 

    public SwissEmptyPlayerList(String message) {
        super(message);
    }
    
    public SwissEmptyPlayerList() {
        super();
    }
    
}
