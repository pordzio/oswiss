package pl.katowice.poldrag.swiss.exceptions;

public class SwissTournamentFinished extends SwissException { 

    public SwissTournamentFinished(String message) {
        super(message);
    }
    
    public SwissTournamentFinished() {
        super();
    }
    
}
