package pl.katowice.poldrag.swiss.exceptions;

import java.lang.Exception;



public class SwissIllegalPlayerName extends SwissException { 

    public SwissIllegalPlayerName(String message) {
        super(message);
    }
    
    public SwissIllegalPlayerName() {
        super();
    }
    
}
