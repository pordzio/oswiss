package pl.katowice.poldrag.swiss.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.core.Result;
import pl.katowice.poldrag.swiss.core.Tournament;
import pl.katowice.poldrag.swiss.gui.tablemodels.PlayerComparator;
import pl.katowice.poldrag.swiss.gui.tablemodels.PlayerTableCellRenderer;
import pl.katowice.poldrag.swiss.gui.tablemodels.StandingsTableModel;
import static pl.katowice.poldrag.swiss.languages.Languages._;

public class StandingsPane extends JPanel
implements ActionListener, TableCardPane, ListSelectionListener
{
    private JButton buttonSave;
    private JButton buttonResume;
    private JButton buttonCard;
    private JButton buttonPrint;
    private JPanel panelButtons;
    private JScrollPane scrollPanePlayers;
    private JTable tablePlayers;
    private StandingsTableModel tableModel;
    
    private TournamentPane tpane;
    private Tournament t;

    public StandingsPane(TournamentPane tpane, Tournament t) {
        this.t = t;
        this.tpane = tpane;
        initComponents();
    }

    @Override
    public void refreshTable() {
        tableModel.fireTableDataChanged();
    }
    
    protected void savePlayerRanking() {
        
        int omitted = 0;
        int errors = 0;
        
        for (Player pl : t.getStandings()) {
            
            if (pl.getFile() == null) {
                omitted++;
                continue;
            }
            
            try {
                Player playerCopy = new Player(pl);
                playerCopy.setRanking(playerCopy.getNewRanking());
                playerCopy.resetPoints();
                FileManager.savePlayer(playerCopy, playerCopy.getFile());                
            } catch (IOException ex) {
                errors++;
            }
        }
        
        if (errors != 0) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_SAVE_RANKING_MSG"),
                _("ERR_SAVE_RANKING_TITLE"),
                JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if (omitted == t.getStandings().size()) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_RANKING_OMITTED_MSG"),
                _("ERR_RANKING_OMITTED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if (omitted != 0) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("INFO_RANKING_OMITTED_MSG"),
                _("INFO_RANKING_OMITTED_TITLE"),
                JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        
        JOptionPane.showMessageDialog(OSwiss.getMainForm(),
            _("INFO_RANKING_SAVED_MSG"),
            _("INFO_RANKING_SAVED_TITLE"),
            JOptionPane.INFORMATION_MESSAGE);
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == buttonSave) {
            savePlayerRanking();
        }

        if (event.getSource() == buttonResume) {
            tpane.resumeTournament();
        }
        
        if (event.getSource() == buttonCard) {
            int ind = tablePlayers.getSelectedRow();
            if (ind < 0)
                return;
            Player pl = t.getStandings().get(tablePlayers.convertRowIndexToModel(ind));
            PlayerCardDialog.showPlayerCard(pl, t.getPlayerPairs(pl));
        }
        
        if (event.getSource() == buttonPrint)
            actionPrint();
    }

    
    private JTable createTablePlayers(List<Player> playerList) {
        
        JTable table = new JTable();
        
        tableModel = new StandingsTableModel(playerList);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setModel(tableModel);
        
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);

        TableColumn tc = table.getColumnModel().getColumn(0);
        tc.setMaxWidth(35);
        tc.setMinWidth(35);
        
        tc = table.getColumnModel().getColumn(2);
        tc.setPreferredWidth(100);
        tc.setMaxWidth(200);
        
        tc = table.getColumnModel().getColumn(3);
        tc.setPreferredWidth(100);
        tc.setMaxWidth(200);
        
        tc = table.getColumnModel().getColumn(4);
        tc.setPreferredWidth(100);
        tc.setMaxWidth(200);
        
        //for (int i=0; i<table.getColumnModel().getColumnCount(); i++)
            table.getColumnModel().getColumn(0).setResizable(false);

        table.getTableHeader().setReorderingAllowed(false);
        
        table.getColumnModel().getColumn(1).setCellRenderer(new PlayerTableCellRenderer());
        table.getColumnModel().getColumn(2).setCellRenderer(dtcr);
        table.getColumnModel().getColumn(3).setCellRenderer(dtcr);
        table.getColumnModel().getColumn(4).setCellRenderer(dtcr);

        // table.setAutoCreateRowSorter(true);
        
        TableRowSorter<StandingsTableModel> sorter 
            = new TableRowSorter<StandingsTableModel>(tableModel);
        table.setRowSorter(sorter);
        sorter.setComparator(1, new PlayerComparator());
        table.getSelectionModel().addListSelectionListener(this);
        return table;
    }

    
    private void initComponents() {

        scrollPanePlayers = new JScrollPane();
        tablePlayers = createTablePlayers(t.getStandings());
        panelButtons = new JPanel();

        setLayout(new java.awt.BorderLayout());

        scrollPanePlayers.setViewportView(tablePlayers);

        add(scrollPanePlayers, java.awt.BorderLayout.CENTER);

        buttonSave = new JButton(_("BUTTON_SAVE_RANKING"),
                OSwiss.getMainForm().getImageIcon("16_save.png"));
        buttonSave.addActionListener(this);
        buttonResume = new JButton(_("BUTTON_RESUME"),
                OSwiss.getMainForm().getImageIcon("16_resume.png"));
        buttonResume.addActionListener(this);
        buttonCard = new JButton(_("BUTTON_PLAYER_CARD"),
                OSwiss.getMainForm().getImageIcon("16_player_card.png"));
        buttonCard.addActionListener(this);
        buttonCard.setEnabled(false);
        
        buttonPrint = new JButton(_("BUTTON_PRINT"),
                OSwiss.getMainForm().getImageIcon("16_print.png"));
        buttonPrint.addActionListener(this);

        javax.swing.GroupLayout panelButtonsLayout = new javax.swing.GroupLayout(panelButtons);
        panelButtons.setLayout(panelButtonsLayout);
        panelButtonsLayout.setHorizontalGroup(
            panelButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonCard, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(buttonSave, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(buttonPrint, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(buttonResume, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelButtonsLayout.setVerticalGroup(
            panelButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonCard)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonResume)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 169, Short.MAX_VALUE)
                .addComponent(buttonPrint)
                .addContainerGap())
        );

        add(panelButtons, java.awt.BorderLayout.LINE_END);

    }


    @Override
    public String toString()
    {
        return _("TITLE_STANDINGS");
    }

    @Override
    public void actionPrint() {
        TablePrinter.printTable(OSwiss.getMainForm().getTabName(tpane), toString(), tablePlayers);
    }

    @Override
    public void setEditable(boolean editable) {
    }

    @Override
    public int getStatus() {
        if (t.getStandings().isEmpty())
            return TableCardPane.STATUS_FAIL;

        return TableCardPane.STATUS_OK;
    }

    @Override
    public void refreshStatus() {
    }

    @Override
    public boolean isAddAllowed() {
        return false;
    }

    @Override
    public boolean isDeleteAllowed() {
        return false;
    }

    @Override
    public boolean isSetResultAllowed() {
        return false;
    }

    @Override
    public boolean isPrintAllowed() {
        return true;
    }

    @Override
    public void actionAdd() {
    }

    @Override
    public void actionDelete() {
    }

    @Override
    public void actionSetResult(Result result) {
    }

    @Override
    public void actionFindPlayer(String playerName) {
        
        int index;
        Player pl;
        boolean found = false;
        
        for (int i=0; i<t.getStandings().size(); i++) {
            
            pl = t.getStandings().get(i);
            index = pl.getName().indexOf(playerName);
            
            if (index >= 0) {
                int rowId = tablePlayers.convertRowIndexToView(i);
                tablePlayers.setRowSelectionInterval(rowId, rowId);
                found = true;
                break;
            }
        }
        
        if (!found)
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("INFO_PLAYER_NOT_FOUND_MSG"),
                _("INFO_PLAYER_NOT_FOUND_TITLE"),
                JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) {
        int ind = tablePlayers.getSelectedRow();
        if (ind < 0 && buttonCard.isEnabled())
            buttonCard.setEnabled(false);
        else if (ind >= 0 && !buttonCard.isEnabled())
            buttonCard.setEnabled(true);
    }

}
