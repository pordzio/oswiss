/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui;

import pl.katowice.poldrag.swiss.core.Result;

/**
 *
 * @author kkopec
 */
public interface TableCardPane {
    
    public static final int STATUS_OK = 0;
    public static final int STATUS_PROGRESS = 1;
    public static final int STATUS_FAIL = 2;
    
    void refreshTable();
        
    void setEditable(boolean editable);
    
    int getStatus();
    
    void refreshStatus();
    
    boolean isAddAllowed();
    boolean isDeleteAllowed();
    boolean isSetResultAllowed();
    // boolean isStartNewRoundAllowed();
    // boolean isEndTournamentAllowed();
    boolean isPrintAllowed();
    
    void actionAdd();
    void actionDelete();
    void actionSetResult(Result result);
    void actionPrint();
    void actionFindPlayer(String playerName);

}
