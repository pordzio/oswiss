/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.StreamException;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import pl.katowice.poldrag.swiss.core.*;

/**
 *
 * @author kkopec
 */
public class FileManager {
    
    public static String EXT_TOURNAMENT = "ost";
    public static String EXT_PLAYER = "osp";


    private FileManager() {
    }
    
    public static String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');
            
        if (i>0 && i<s.length() - 1)
            ext = s.substring(i+1).toLowerCase();
            
        return ext;
    }
    
    public static File addExtensionIfNeeded(File file, String extension) {
        
        String ext = getExtension(file);
        String path = file.getPath();

        if (ext == null)
            return new File(path + "." + extension);
        
        if (!ext.equals(extension)) {
            int lastDot = path.lastIndexOf(".");
            
            return new File(path.substring(0, lastDot+1) + extension);
        }
            
        return file;
    }
    
    private static XStream getXStream() {
        XStream xStream = new XStream(new DomDriver());
        xStream.alias("swiss", Swiss.class);
        xStream.alias("player", Player.class);
        xStream.alias("round", Round.class);
        xStream.alias("pair", Pair.class);
        
        xStream.autodetectAnnotations(true);
        return xStream;
    }
    
    public static void saveTournament(Tournament t, File file) throws IOException
    {
        XStream xStream = getXStream();

        BufferedWriter out = new BufferedWriter(new FileWriter(file));
        out.write(xStream.toXML(t));
        out.close();
    }
    
    public static Tournament openTournament(File file)
            throws StreamException
    {
        XStream xStream = getXStream();
        Tournament t = (Tournament)xStream.fromXML(file);
        
        // changing instance of PAUSE and MISS players to Player.playerPause
        for(int i=0; i<t.getRoundsCount(); i++) {
            Round r = t.getRound(i);
            
            for (Pair p : r.getPairs()) {
                
                if (p.getPlayer1().getPublicId() == Player.PLAYER_ID_PAUSE)
                    p.setPlayer1(Player.playerPause);

                if (p.getPlayer1().getPublicId() == Player.PLAYER_ID_MISS)
                    p.setPlayer1(Player.playerMiss);
                
                if (p.getPlayer2().getPublicId() == Player.PLAYER_ID_PAUSE)
                    p.setPlayer2(Player.playerPause);

                if (p.getPlayer2().getPublicId() == Player.PLAYER_ID_MISS)
                    p.setPlayer2(Player.playerMiss);
            }
        }
        
        return t;
    }
    
    
    public static void savePlayer(Player pl, File file) throws IOException
    {
        XStream xStream = getXStream();

        BufferedWriter out = new BufferedWriter(new FileWriter(file));
        out.write(xStream.toXML(pl));
        out.close();
    }
    
    public static Player openPlayer(File file)
            throws StreamException
    {
        XStream xStream = getXStream();
        return (Player)xStream.fromXML(file);
    }
    
}
