/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
import pl.katowice.poldrag.swiss.core.Pair;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.core.Result;
import pl.katowice.poldrag.swiss.core.Round;
import pl.katowice.poldrag.swiss.gui.tablemodels.PairTableModel;
import static pl.katowice.poldrag.swiss.languages.Languages._;

/**
 *
 * @author kkopec
 */
public class PairListPane extends JPanel
implements ActionListener, TableCardPane, ListSelectionListener {

    // Variables declaration - do not modify
    private JButton buttonAddPair;
    private JButton buttonClearResult;
    private JButton buttonDelPair;
    private JButton buttonSetResult;
    private JButton buttonPrint;
    private JPanel panelButtons;
    private JScrollPane scrollPaneRounds;
    private JTable tablePairs;
    private PairTableModel tableModel;
    
    // End of variables declaration
    TournamentPane tpane;
    private Round round;
    
    private int validationStatus = TableCardPane.STATUS_PROGRESS;

    public PairListPane(TournamentPane tpane, Round round) {
        this.tpane = tpane;
        this.round = round;
        updateStatus();
        initComponents();
    }
    
    @Override
    public void refreshTable() {
        tableModel.fireTableDataChanged();
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private JTable createTablePairs(List<Pair> pairList) {
        
        JTable table = new JTable();
        
        tableModel = new PairTableModel(tpane, this, pairList);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setModel(tableModel);
        
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);

        TableColumn tc = table.getColumnModel().getColumn(0);
        tc.setMaxWidth(35);
        tc.setMinWidth(35);
                
        tc = table.getColumnModel().getColumn(3);
        tc.setPreferredWidth(100);
        tc.setMaxWidth(200);
        
        table.getColumnModel().getColumn(0).setResizable(false);
        table.getTableHeader().setReorderingAllowed(false);
        
        table.getColumnModel().getColumn(1).setCellRenderer(dtcr);
        table.getColumnModel().getColumn(2).setCellRenderer(dtcr);
        table.getColumnModel().getColumn(3).setCellRenderer(dtcr);
        
        TableRowSorter<PairTableModel> sorter 
            = new TableRowSorter<PairTableModel>(tableModel);
        table.setRowSorter(sorter);
        table.getSelectionModel().addListSelectionListener(this);
        return table;
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        scrollPaneRounds = new JScrollPane();
        tablePairs = createTablePairs(round.getPairs());
        panelButtons = new JPanel();
        buttonClearResult = new JButton();
        buttonSetResult = new JButton();

        setLayout(new java.awt.BorderLayout());

        scrollPaneRounds.setViewportView(tablePairs);

        add(scrollPaneRounds, java.awt.BorderLayout.CENTER);

        buttonSetResult.setText(_("BUTTON_SET_RESULT"));
        buttonSetResult.addActionListener(this);

        buttonClearResult.setText(_("BUTTON_CLEAR_RESULT"));
        buttonClearResult.addActionListener(this);

        buttonAddPair = new JButton(_("BUTTON_ADD_PAIR"),
                OSwiss.getMainForm().getImageIcon("16_add.png"));
        buttonAddPair.addActionListener(this);

        buttonDelPair = new JButton(_("BUTTON_DELETE_PAIR"),
                OSwiss.getMainForm().getImageIcon("16_del.png"));
        buttonDelPair.addActionListener(this);

        buttonPrint = new JButton(_("BUTTON_PRINT"),
                OSwiss.getMainForm().getImageIcon("16_print.png"));
        buttonPrint.addActionListener(this);

        GroupLayout panelButtonsLayout = new GroupLayout(panelButtons);
        panelButtons.setLayout(panelButtonsLayout);
        panelButtonsLayout.setHorizontalGroup(
            panelButtonsLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panelButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelButtonsLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(buttonClearResult, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(buttonSetResult, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .addComponent(buttonAddPair, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .addComponent(buttonDelPair, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                    .addComponent(buttonPrint, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelButtonsLayout.setVerticalGroup(
            panelButtonsLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(panelButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonSetResult)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonClearResult)
                .addGap(18, 18, 18)
                .addComponent(buttonAddPair)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonDelPair)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 133, Short.MAX_VALUE)
                .addComponent(buttonPrint)
                .addContainerGap())
        );

        add(panelButtons, java.awt.BorderLayout.LINE_END);
    }// </editor-fold>
    
    
    @Override
    public String toString()
    {
        if (round.isTop())
            return _("TITLE_TOP_ROUND")+ " " + String.valueOf(round.getTop());

        return _("TITLE_ROUND")+ " " + String.valueOf(round.getNumber());
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == buttonSetResult) {
            
            int ind = tablePairs.getSelectedRow();
            if (ind == -1)
                return;
            
            SetResultDialog srd = new SetResultDialog();

            if (srd.execute()) {
                round.getPair(tablePairs.convertRowIndexToModel(ind)).setPoints(srd.getResult());
                refreshTable();
                tpane.refreshStatus(this);
            }
            tablePairs.setRowSelectionInterval(ind, ind);
            
        }
        
        if (ae.getSource() == buttonClearResult) {
            
            int ind = tablePairs.getSelectedRow();
            if (ind == -1)
                return;
            
            round.getPair(tablePairs.convertRowIndexToModel(ind)).setPoints(null);
            refreshTable();
            tpane.refreshStatus(this);
            tablePairs.setRowSelectionInterval(ind, ind);
        }
        
        if (ae.getSource() == buttonDelPair) {
            
            int ind = tablePairs.getSelectedRow();
            if (ind == -1)
                return;
            
            round.getPairs().remove(tablePairs.convertRowIndexToModel(ind));
            refreshTable();
            tpane.refreshStatus(this);
        }
        
        if (ae.getSource() == buttonAddPair) {
            
            Pair p = AddPairDialog.showAddPairDialog(tpane.getTournament(), round);
            if (p == null)
                return;
            round.addPair(p);
            refreshTable();
            tpane.refreshStatus(this);
            int rowId = tablePairs.convertRowIndexToView(round.getPairs().size()-1);
            tablePairs.setRowSelectionInterval(rowId, rowId);
        }
        
        if (ae.getSource() == buttonPrint)
            actionPrint();
        
    }

    @Override
    public void actionPrint() {
        TablePrinter.printTable(OSwiss.getMainForm().getTabName(tpane), toString(), tablePairs);
    }

    @Override
    public void setEditable(boolean editable) {
        tableModel.setEditable(editable);
    }

    @Override
    public int getStatus() {
        return validationStatus;
    }
    
    @Override
    public void refreshStatus() {
        updateStatus();
    }
    
    private void updateStatus() {
        
        List<Player> playerList = tpane.getTournament().getPlayerList();
        if (!round.isTop()) {
            // swiss round - all players must be listed in pairings
            for (Player pl : playerList) {
                if (round.getPairWithPlayer(pl) == null) {
                    validationStatus = TableCardPane.STATUS_FAIL;
                    return;
                }
            }
        } else {
            // top round - only top players are listed
            int numOfPairs = round.getPairs().size();
            if ((numOfPairs & (numOfPairs-1)) != 0) {
                validationStatus = TableCardPane.STATUS_FAIL;
                return;
            }
        }
        
        if (!round.allResultsFilled()) {
            validationStatus = TableCardPane.STATUS_PROGRESS;
            return;
        }
        
        validationStatus = TableCardPane.STATUS_OK;
    }

    @Override
    public boolean isAddAllowed() {
        if (tpane.getTournament().isFinished()) {
            buttonAddPair.setEnabled(false);
            return false;
        }
        
        if (round.isTop() &&
           (tpane.getTournament().currentRound() != round ||
           tpane.getTournament().findInitialTop() != round.getTop())) {
            buttonAddPair.setEnabled(false);
            return false;
        }
        
        buttonAddPair.setEnabled(true);
        return true;
    }

    @Override
    public boolean isDeleteAllowed() {
        if (tpane.getTournament().isFinished()) {
            buttonDelPair.setEnabled(false);
            return false;
        }
        
        int ind = tablePairs.getSelectedRow();
        if (ind == -1) {
            buttonDelPair.setEnabled(false);
            return false;
        }
        
        if (round.isTop() &&
           (tpane.getTournament().currentRound() != round ||
           tpane.getTournament().findInitialTop() != round.getTop())) {
            buttonDelPair.setEnabled(false);
            return false;
        }
    
        buttonDelPair.setEnabled(true);
        return true;
    }

    @Override
    public boolean isSetResultAllowed() {
        boolean b;
        int ind = tablePairs.getSelectedRow();
        
        if (tpane.getTournament().isFinished()) {
            buttonDelPair.setEnabled(false);
            b = false;
        } else if (ind == -1) {
            buttonDelPair.setEnabled(false);
            b = false;
        } else
            b = true;
    
        buttonSetResult.setEnabled(b);
        buttonClearResult.setEnabled(b);
        return b;
    }

    @Override
    public boolean isPrintAllowed() {
        return true;
    }

    @Override
    public void actionAdd() {
        this.actionPerformed(new ActionEvent(buttonAddPair, 0, null));
    }

    @Override
    public void actionDelete() {
        this.actionPerformed(new ActionEvent(buttonDelPair, 0, null));
    }

    @Override
    public void actionSetResult(Result result) {
        int ind = tablePairs.getSelectedRow();
        if (ind == -1)
            return;
            
        round.getPair(tablePairs.convertRowIndexToModel(ind)).setPoints(result);
        refreshTable();
        tpane.refreshStatus(this);
        tablePairs.setRowSelectionInterval(ind, ind);
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) {
        OSwiss.getMainForm().refreshEnableIcons();
    }

    @Override
    public void actionFindPlayer(String playerName) {
        
        int index;
        Pair p;
        boolean found = false;
        
        for (int i=0; i<round.getPairs().size(); i++) {
            
            p = round.getPairs().get(i);
            index = p.getPlayer1().getName().indexOf(playerName);
            if (index < 0)
                index = p.getPlayer2().getName().indexOf(playerName);
            
            if (index >= 0) {
                int rowId = tablePairs.convertRowIndexToView(i);
                tablePairs.setRowSelectionInterval(rowId, rowId);
                found = true;
                break;
            }
        }
        
        if (!found)
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("INFO_PLAYER_NOT_FOUND_MSG"),
                _("INFO_PLAYER_NOT_FOUND_TITLE"),
                JOptionPane.INFORMATION_MESSAGE);
    }
}
