/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui;

import java.awt.print.PrinterException;
import java.text.MessageFormat;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static pl.katowice.poldrag.swiss.languages.Languages._;

/**
 *
 * @author kkopec
 */
public class TablePrinter {
    
    private TablePrinter() {
    }

    public static boolean printTable(
            String tournamentName,
            String title,
            JTable table)
    {
        MessageFormat header = new MessageFormat(tournamentName + " - " + title);
        MessageFormat footer = new MessageFormat(_("TABLE_FOOTER"));

        JTable.PrintMode mode = JTable.PrintMode.FIT_WIDTH;
        boolean complete = false;
        
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        if (printServices.length == 0) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_PRINTER_MSG"),
                _("ERR_NO_PRINTER_TITLE"),
                JOptionPane.ERROR_MESSAGE);
            return false;
        }
        
        try {
            /* print the table */
            complete = table.print(mode, header, footer,
                    true, null, true, null);

            /* if printing completes */
            if (complete) {
                /* show a success message */
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                        _("INFO_PRINT_OK_MSG"),
                        _("INFO_PRINT_OK_TITLE"),
                        JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (PrinterException pe) {
            /* Printing failed, report to the user */
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_PRINT_FAIL_MSG") + " " + pe.getMessage(),
                _("INFO_PRINT_FAIL_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        }
        
        return complete;
    }
    
    
}
