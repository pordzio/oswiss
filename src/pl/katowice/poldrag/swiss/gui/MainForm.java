package pl.katowice.poldrag.swiss.gui;

import com.thoughtworks.xstream.io.StreamException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.core.Result;
import pl.katowice.poldrag.swiss.core.Swiss;
import pl.katowice.poldrag.swiss.core.Tournament;
import static pl.katowice.poldrag.swiss.languages.Languages._;


public final class MainForm extends JFrame
implements ActionListener, ChangeListener
{
    private static String FORM_TITLE;;

    public static Object[] optionsYesNo;

    private JTabbedPane tabbedPane;
    private JMenuItem menuItemNewTournament = null;
    private JMenuItem menuItemOpen = null;
    private JMenuItem menuItemSave = null;
    private JMenuItem menuItemSaveAs = null;
    private JMenuItem menuItemClose = null;
    private JMenuItem menuItemOptions = null;
    private JMenuItem menuItemQuit = null;

    private JMenuItem menuNewRound = null;
    private JMenuItem menuDelRound = null;
    private JMenuItem menuStartTop = null;
    private JMenuItem menuEndTournament = null;
    private JMenuItem menuResumeTournament = null;

    private JMenuItem menuCreatePlayer = null;
    private JMenuItem menuEditPlayer = null;
    private JMenuItem menuFindPlayer = null;

    private JMenuItem menuPrintPlayerList = null;
    private JMenuItem menuPrintCurrentRound = null;

    private JMenuItem menuAbout = null;

    private JButton toolNew = null;
    private JButton toolOpen = null;
    private JButton toolSave = null;
    private JButton toolAdd = null;
    private JButton toolDel = null;
    private JButton toolWin = null;
    private JButton toolDraw = null;
    private JButton toolLoss = null;
    private JButton toolNewRound = null;
    private JButton toolEndTournament = null;
    private JButton toolPrintCurrentRound = null;

    private final static int MIN_WINDOW_WIDTH = 640;
    private final static int MIN_WINDOW_HEIGHT = 480;

    public static File confDirPlayer;

    JFileChooser fileChooserTournament = null;
    JFileChooser fileChooserPlayer = null;

    protected File localPathPlayer = null;

    // tabbedPane selection changed
    @Override
    public void stateChanged(ChangeEvent ce) {
         refreshEnableIcons();
    }

    class ExtensionFilter extends FileFilter {

        private String description;
        private String extensions[];

        public ExtensionFilter(String description, String extensions[]) {
            this.description = description;
            this.extensions = extensions;
        }

        @Override
        public boolean accept(File f) {

            if (f.isDirectory())
                return true;

            String extension = FileManager.getExtension(f);
            if (extension != null) {
                boolean accept = false;

                for (String ext : extensions)
                    if (extension.equals(ext)) {
                        accept = true;
                        break;
                    }

                return accept;
            }

            return false;
        }


        @Override
        public String getDescription() {
            return description;
        }

    }

    /** Creates new form MainForm */
    public MainForm() {

        FORM_TITLE = _("MAINFORM_TITLE");
        optionsYesNo = new String[] {_("BUTTON_YES"), _("BUTTON_NO")};

        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) { }

        setMinimumSize(new Dimension(MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.setTitle(FORM_TITLE);
        setJMenuBar(createMenuBar());

        getContentPane().add(createToolBar(), BorderLayout.NORTH);

        tabbedPane = new JTabbedPane();
        tabbedPane.addChangeListener(this);

        fileChooserTournament = new JFileChooser();
        fileChooserTournament.setAcceptAllFileFilterUsed(false);
        fileChooserTournament.addChoosableFileFilter(new ExtensionFilter(
                _("MAINFORM_OST_FILE_FILTER"), new String[] {FileManager.EXT_TOURNAMENT}));

        fileChooserPlayer = new JFileChooser();
        fileChooserPlayer.setAcceptAllFileFilterUsed(false);
        fileChooserPlayer.addChoosableFileFilter(new ExtensionFilter(
                _("MAINFORM_OSP_FILE_FILTER"), new String[] {FileManager.EXT_PLAYER}));

        getContentPane().add(tabbedPane, BorderLayout.CENTER);

        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {

                if (OSwiss.getMainForm().getCurrentTournament() == null) {
                    OSwiss.getMainForm().dispose();
                    return;
                }

                int confirmed = JOptionPane.showConfirmDialog(OSwiss.getMainForm(),
                _("ASK_QUIT_MSG"), _("ASK_QUIT_TITLE"),
                JOptionPane.YES_NO_OPTION);
                if (confirmed == JOptionPane.YES_OPTION)
                    OSwiss.getMainForm().dispose();
            }
        });

        pack();

        refreshEnableIcons();
        setVisible(true);
    }


    /* tworzy nowa zakladke turnieju */
    private void startNewTournament()
    {
        String name;

        name = JOptionPane.showInputDialog(this, _("DIALOG_TOURNAMENT_MSG"), _("DIALOG_TOURNAMENT_NAME"));
        if (name == null || name.equals(""))
            return;

        Tournament t = new Swiss(name);
        TournamentPane tpane = new TournamentPane(t);
        tabbedPane.addTab(name, null, tpane, null);
        tabbedPane.setTabComponentAt(
                tabbedPane.indexOfComponent(tpane),
                new TabTitlePane(tabbedPane));
        tabbedPane.setSelectedComponent(tpane);
    }


    protected TournamentPane getCurrentTournament() {
        int selectedIndex = tabbedPane.getSelectedIndex();
        if (selectedIndex >= 0)
            return (TournamentPane)tabbedPane.getComponentAt(selectedIndex);
        return null;
    }




    private void openTournament() {
        try {
            TournamentPane tpane = new TournamentPane();
            tabbedPane.addTab(tpane.getTournamentFile().getName(), null, tpane, null);
            tabbedPane.setTabComponentAt(
                    tabbedPane.indexOfComponent(tpane),
                    new TabTitlePane(tabbedPane));
            tabbedPane.setSelectedComponent(tpane);
        } catch (FileNotFoundException ex) {
          // operation cancelled by user
        } catch (StreamException ex) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_FILE_CORRUPT_MSG"),
                _("ERR_FILE_CORRUPT_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        }
    }

    private void createPlayer() {

        Player p = EditPlayerDialog.showCreatePlayerDialog();

        if (p == null || p.getName().equals(""))
            return;

        File fileName;
        if (p.getFile() != null) {
            fileName = p.getFile();
        } else {
            fileName = FileManager.addExtensionIfNeeded(
                    new File(p.getName()), FileManager.EXT_PLAYER);
        }

        if (localPathPlayer == null) {
            localPathPlayer = confDirPlayer;
        }
        fileChooserPlayer.setCurrentDirectory(localPathPlayer);
        fileChooserPlayer.setSelectedFile(fileName);

        int ret = fileChooserPlayer.showSaveDialog(this);

        if (ret == JFileChooser.APPROVE_OPTION) {
            try {
                File file = FileManager.addExtensionIfNeeded(
                        fileChooserPlayer.getSelectedFile(), FileManager.EXT_PLAYER);

                int n = JOptionPane.YES_OPTION;
                if (file.exists())
                    n = JOptionPane.showOptionDialog(
                        OSwiss.getMainForm(),
                        _("ASK_REPLACE_MSG"),
                        _("ASK_REPLACE_TITLE"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        MainForm.optionsYesNo,
                        MainForm.optionsYesNo[0]);

                if (n == JOptionPane.YES_OPTION) {
                    p.setFile(file);
                    FileManager.savePlayer(p, file);
                    localPathPlayer = new File(file.getPath());
                }

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("ERR_SAVE_MSG"),
                    _("ERR_SAVE_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
            }
        }
    }


    public Player openPlayer() {

        if (localPathPlayer == null) {
            localPathPlayer = confDirPlayer;
        }
        fileChooserPlayer.setCurrentDirectory(localPathPlayer);
        int ret = fileChooserPlayer.showOpenDialog(this);
        Player p = null;

        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileChooserPlayer.getSelectedFile();
            try {
                p = FileManager.openPlayer(file);
                p.setFile(file);
                localPathPlayer = new File(file.getPath());
            } catch (StreamException se) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("ERR_FILE_CORRUPT_MSG"),
                    _("ERR_FILE_CORRUPT_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
            }
        }

        return p;
    }

    private void editPlayer() {
        Player p = openPlayer();
        if (p != null) {
            if (EditPlayerDialog.showEditPlayerDialog(p)) {
                try {
                    FileManager.savePlayer(p, p.getFile());

                    JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                            _("INFO_PLAYER_SAVED_MSG"),
                            _("INFO_PLAYER_SAVED_TITLE"),
                            JOptionPane.INFORMATION_MESSAGE);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                        _("ERR_SAVE_MSG"),
                        _("ERR_SAVE_TITLE"),
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    public void setTabName(TournamentPane tpane, String newName)
    {
        int index = tabbedPane.indexOfComponent(tpane);
        tabbedPane.setTitleAt(index, newName);
    }

    public String getTabName(TournamentPane tpane)
    {
        int index = tabbedPane.indexOfComponent(tpane);
        return tabbedPane.getTitleAt(index);
    }

    public JFileChooser getFileChooserPlayer() {
        return fileChooserPlayer;
    }

    public JFileChooser getFileChooserTournament() {
        return fileChooserTournament;
    }

    public void refreshEnableIcons() {
        int selectedIndex = tabbedPane.getSelectedIndex();
        TournamentPane tpane;

        if (selectedIndex < 0) {
            menuItemSave.setEnabled(false);
            menuItemSaveAs.setEnabled(false);
            menuItemClose.setEnabled(false);
            toolSave.setEnabled(false);
            menuNewRound.setEnabled(false);
            menuStartTop.setEnabled(false);
            menuDelRound.setEnabled(false);
            menuEndTournament.setEnabled(false);
            menuResumeTournament.setEnabled(false);
            menuFindPlayer.setEnabled(false);
            menuPrintPlayerList.setEnabled(false);
            menuPrintCurrentRound.setEnabled(false);
            toolAdd.setEnabled(false);
            toolDel.setEnabled(false);
            toolWin.setEnabled(false);
            toolDraw.setEnabled(false);
            toolLoss.setEnabled(false);
            toolNewRound.setEnabled(false);
            toolEndTournament.setEnabled(false);
            toolPrintCurrentRound.setEnabled(false);
            return;
        } else {
            tpane = (TournamentPane)tabbedPane.getComponentAt(selectedIndex);

            menuItemSave.setEnabled(true);
            menuItemSaveAs.setEnabled(true);
            menuItemClose.setEnabled(true);
            toolSave.setEnabled(true);

            Tournament t = tpane.getTournament();
            boolean b = !t.isFinished();
            menuStartTop.setEnabled(b && (t.currentRound() == null || !t.currentRound().isTop()));
            menuNewRound.setEnabled(b);
            menuDelRound.setEnabled(b);
            menuEndTournament.setEnabled(b);
            menuResumeTournament.setEnabled(!b);
            menuFindPlayer.setEnabled(true);
            menuPrintPlayerList.setEnabled(true);
            toolNewRound.setEnabled(b);
            toolEndTournament.setEnabled(b);
        }

        TableCardPane tcp = (TableCardPane)tpane.getCurrentPane();

        if (tcp == null) {
            menuPrintCurrentRound.setEnabled(false);
            toolAdd.setEnabled(false);
            toolDel.setEnabled(false);
            toolWin.setEnabled(false);
            toolDraw.setEnabled(false);
            toolLoss.setEnabled(false);
            toolNewRound.setEnabled(false);
            toolEndTournament.setEnabled(false);
            toolPrintCurrentRound.setEnabled(false);
            return;
        }

        boolean b = tcp.isPrintAllowed();
        menuPrintCurrentRound.setEnabled(b);
        toolPrintCurrentRound.setEnabled(b);
        toolAdd.setEnabled(tcp.isAddAllowed());
        toolDel.setEnabled(tcp.isDeleteAllowed());

        b = tcp.isSetResultAllowed();
        toolWin.setEnabled(b);
        toolDraw.setEnabled(b);
        toolLoss.setEnabled(b);
    }

    private void closeTournament() {
        int selectedIndex = tabbedPane.getSelectedIndex();

        if (selectedIndex >= 0) {
            boolean confirmed =
                    ((TournamentPane)tabbedPane.getComponentAt(selectedIndex)).closeTournament();
            if (confirmed) {
                tabbedPane.remove(selectedIndex);
            }
        }
    }


    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == menuItemNewTournament ||
            event.getSource() == toolNew)
            startNewTournament();

        if (event.getSource() == menuItemOpen ||
            event.getSource() == toolOpen)
            openTournament();

        if (event.getSource() == menuItemSaveAs)
            getCurrentTournament().saveTournamentAs();

        if (event.getSource() == menuItemSave ||
            event.getSource() == toolSave)
            getCurrentTournament().saveTournament();

        if (event.getSource() == menuItemClose)
            closeTournament();

        if (event.getSource() == menuItemOptions)
            OptionsDialog.showOptionsDialog();

        if (event.getSource() == menuItemQuit) {
            System.exit(0);
        }

        if (event.getSource() == menuNewRound ||
            event.getSource() == toolNewRound) {
            getCurrentTournament().startNewRound();
            refreshEnableIcons();
        }

        if (event.getSource() == menuStartTop) {
            getCurrentTournament().startTopRound();
            refreshEnableIcons();
        }

        if (event.getSource() == menuDelRound) {
            getCurrentTournament().deleteLastRound();
            refreshEnableIcons();
        }

        if (event.getSource() == menuEndTournament ||
            event.getSource() == toolEndTournament) {
            getCurrentTournament().endTournament();
            refreshEnableIcons();
        }

        if (event.getSource() == menuAbout)
            AboutDialog.showAboutDialog();

        if (event.getSource() == menuResumeTournament) {
            getCurrentTournament().resumeTournament();
            refreshEnableIcons();
        }

        if (event.getSource() == menuCreatePlayer) {
            createPlayer();
        }

        if (event.getSource() == menuEditPlayer) {
            editPlayer();
        }

        if (event.getSource() == menuFindPlayer) {

            String name;
            name = JOptionPane.showInputDialog(this, _("DIALOG_PLAYER_NAME"), "");
            if (name == null || name.equals(""))
                return;
            getCurrentTournament().getCurrentPane().actionFindPlayer(name);
        }

        if (event.getSource() == toolAdd) {
            getCurrentTournament().getCurrentPane().actionAdd();
            refreshEnableIcons();
        }

        if (event.getSource() == toolDel) {
            getCurrentTournament().getCurrentPane().actionDelete();
            refreshEnableIcons();
        }

        if (event.getSource() == toolWin)
            getCurrentTournament().getCurrentPane().actionSetResult(new Result(Result.resultWin));

        if (event.getSource() == toolDraw) {
            getCurrentTournament().getCurrentPane().actionSetResult(new Result(Result.resultDraw));
        }

        if (event.getSource() == toolLoss) {
            getCurrentTournament().getCurrentPane().actionSetResult(new Result(Result.resultLoss));
        }

        if (event.getSource() == menuPrintCurrentRound ||
            event.getSource() == toolPrintCurrentRound) {
            getCurrentTournament().getCurrentPane().actionPrint();
        }

        if (event.getSource() == menuPrintPlayerList)
            getCurrentTournament().printPlayerList();

    }

    protected JMenuItem makeMenuItem(String name,
            String imageName, KeyStroke accelerator) {

        JMenuItem menuItem = new JMenuItem(name);
        // mnemonic is against i18n
        //if (mnemonic != 0)
        //    menuItem.setMemonic(mnemonic);
        if (imageName != null)
            menuItem.setIcon(getImageIcon(imageName));
        if (accelerator != null)
            menuItem.setAccelerator(accelerator);

        menuItem.addActionListener(this);

        return menuItem;
    }

    @SuppressWarnings("unchecked")
    protected JMenuBar createMenuBar()
    {
        JMenuBar menuBar;
        JMenu menu;

        // create an empty menubar
        menuBar = new JMenuBar();


        // 1st menu - File column
        menu = new JMenu(_("MENU_FILE"));
        menuBar.add(menu);

        menu.add(menuItemNewTournament = makeMenuItem(_("MENU_NEW"), "16_new.png",
                KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK)));
        menu.add(menuItemOpen = makeMenuItem(_("MENU_OPEN"), "16_open.png",
                KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK)));
        menu.add(menuItemSave = makeMenuItem(_("MENU_SAVE"), "16_save.png",
                KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK)));
        menu.add(menuItemSaveAs = makeMenuItem(_("MENU_SAVE_AS"), "16_save_as.png", null));
        menu.add(menuItemClose = makeMenuItem(_("MENU_CLOSE"), "16_close.png", null));
        menu.addSeparator();
        menu.add(menuItemOptions = makeMenuItem(_("MENU_OPTIONS"), "16_options.png", null));
        menu.add(menuItemQuit = makeMenuItem(_("MENU_QUIT"), "16_exit.png",
                KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK)));

        // 2nd menu - Tournament column
        menu = new JMenu(_("MENU_TOURNAMENT"));
        menuBar.add(menu);
        menu.add(menuNewRound = makeMenuItem(_("MENU_NEXT_ROUND"), "16_start.png",
                KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK)));
        menu.add(menuEndTournament = makeMenuItem(_("MENU_END_TOURNAMENT"), "16_end.png", null));
        menu.add(menuDelRound = makeMenuItem(_("MENU_DELETE_LAST_ROUND"), "16_delete_round.png", null));
        menu.add(menuStartTop = makeMenuItem(_("MENU_START_TOP"), "16_start_top.png",null));
        menu.add(menuResumeTournament = makeMenuItem(_("MENU_RESUME_TOURNAMENT"), "16_resume.png", null));
        // moved to version 2.0
        // menu.add(menuStartTop = makeMenuItem("Start TOP", "16_top.png", null));

        // 3rd menu - Player column
        menu = new JMenu(_("MENU_PLAYER"));
        menuBar.add(menu);
        menu.add(menuCreatePlayer = makeMenuItem(_("MENU_CREATE_PLAYER"), "16_create_player.png", null));
        menu.add(menuEditPlayer = makeMenuItem(_("MENU_EDIT_PLAYER"), "16_edit_player.png", null));
        menu.add(menuFindPlayer = makeMenuItem(_("MENU_FIND_PLAYER"), "16_find_player.png",
                KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK)));

        // 4th menu - Print column
        menu = new JMenu(_("MENU_PRINT"));
        menuBar.add(menu);
        menu.add(menuPrintPlayerList = makeMenuItem(_("MENU_PRINT_PLAYERS"), null, null));
        menu.add(menuPrintCurrentRound = makeMenuItem(_("MENU_PRINT_CURRENT"), "16_print.png",
                KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK)));

        // 5th menu - Help column
        menu = new JMenu(_("MENU_HELP"));
        menuBar.add(menu);
        menu.add(menuAbout = makeMenuItem(_("MENU_ABOUT"), "16_about.png", null));

        return menuBar;
    }


    public ImageIcon getImageIcon(String fileName) {

        return new ImageIcon(
                getClass().getResource("/pl/katowice/poldrag/swiss/icons/" + fileName)
        );
    }


    protected JButton makeToolBarButton(String toolTip, String imageName)
    {
        JButton button;

        button = new JButton(getImageIcon(imageName));
        button.setOpaque(false);
        button.setBorderPainted(false);
        button.setFocusPainted(false);

        if (toolTip != null)
            button.setToolTipText(toolTip);

        button.addActionListener(this);
        return button;
    }


    protected JToolBar createToolBar() {

        JToolBar toolBar = new JToolBar(_("TOOLBAR_TITLE"));
        toolBar.setFloatable(false);

        toolBar.add(toolNew = makeToolBarButton(_("TOOLBAR_NEW_TOOLTIP"), "22_new.png"));
        toolBar.add(toolOpen = makeToolBarButton(_("TOOLBAR_OPEN_TOOLTIP"), "22_open.png"));
        toolBar.add(toolSave = makeToolBarButton(_("TOOLBAR_SAVE_TOOLTIP"), "22_save.png"));
        toolBar.addSeparator(new Dimension(20, 22));

        toolBar.add(toolAdd = makeToolBarButton(_("TOOLBAR_ADD_TOOLTIP"), "22_add.png"));
        toolBar.add(toolDel = makeToolBarButton(_("TOOLBAR_DELETE_TOOLTIP"), "22_del.png"));
        toolBar.addSeparator(new Dimension(20, 22));

        toolBar.add(toolWin = makeToolBarButton(_("TOOLBAR_SET_WIN_TOOLTIP"), "22_win.png"));
        toolBar.add(toolDraw = makeToolBarButton(_("TOOLBAR_SET_DRAW_TOOLTIP"), "22_draw.png"));
        toolBar.add(toolLoss = makeToolBarButton(_("TOOLBAR_SET_LOSS_TOOLTIP"), "22_loss.png"));
        toolBar.addSeparator(new Dimension(20, 22));

        toolBar.add(toolNewRound = makeToolBarButton(_("TOOLBAR_START_TOOLTIP"), "22_start.png"));
        toolBar.add(toolEndTournament = makeToolBarButton(_("TOOLBAR_END_TOOLTIP"), "22_end.png"));
        toolBar.add(toolPrintCurrentRound = makeToolBarButton(_("TOOLBAR_PRINT_TOOLTIP"), "22_print.png"));

        return toolBar;
    }

}
