package pl.katowice.poldrag.swiss.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;
import pl.katowice.poldrag.swiss.core.*;
import pl.katowice.poldrag.swiss.exceptions.SwissIllegalPlayerName;
import pl.katowice.poldrag.swiss.exceptions.SwissPlayerNameCollision;
import pl.katowice.poldrag.swiss.exceptions.SwissTournamentFinished;
import pl.katowice.poldrag.swiss.gui.tablemodels.PlayerComparator;
import pl.katowice.poldrag.swiss.gui.tablemodels.PlayerTableCellRenderer;
import pl.katowice.poldrag.swiss.gui.tablemodels.PlayerTableModel;
import static pl.katowice.poldrag.swiss.languages.Languages._;

public class PlayerListPane extends JPanel
implements ActionListener, TableCardPane, ListSelectionListener
{
    private JButton buttonAdd;
    private JButton buttonDel;
    private JButton buttonOpen;
    private JButton buttonPrint;
    private JPanel panelButtons;
    private JScrollPane scrollPanePlayers;
    private JTable tablePlayers;
    private PlayerTableModel tableModel;
    
    TournamentPane tpane;
    private Tournament t;

    public PlayerListPane(TournamentPane tpane, Tournament t) {
        this.t = t;
                this.tpane = tpane;
        initComponents();
    }

    @Override
    public void refreshTable() {
        tableModel.fireTableDataChanged();
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == buttonPrint)
            actionPrint();
        
        
        if (event.getSource() == buttonAdd ||
            event.getSource() == buttonOpen) {
            
            if (t.isFinished()) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("ERR_FINISHED_MSG"),
                    _("ERR_FINISHED_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
                
                return;
            }
            
            Player player = null;
            if (event.getSource() == buttonAdd)
                player = EditPlayerDialog.showAddPlayerDialog(t.getPlayerList());
            else if (event.getSource() == buttonOpen)
                player = OSwiss.getMainForm().openPlayer();
            
            if (player == null)
                return;

            try {
                boolean missAdded = t.addPlayer(player);
                
                if (missAdded) {

                    // does should i replace miss with pause player?
                    int n = JOptionPane.showOptionDialog(
                        OSwiss.getMainForm(),
                        _("ASK_CAN_PLAY_MSG"),
                        _("ASK_CAN_PLAY_TITLE"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,     //do not use a custom Icon
                        MainForm.optionsYesNo,  //the titles of buttons
                        MainForm.optionsYesNo[0]); //default button title

                    if (n == JOptionPane.YES_OPTION) {

                        // add pair Player-PAUSE or Player-Player2
                        // where Player2 was the player with PAUSE
                        // delete pair (player - MISS)
                        
                        Round r = t.currentRound();
                        
                        // fetch player from the last pair ( pl - miss ).
                        // it may be different from given player if he goes back to play
                        // from status "leaved"
                        player = r.getPair(r.getPairs().size()-1).getPlayer1();
                        r.removeLastPair();
                        Player opponent = Player.playerPause;

                        for (Pair p : r.getPairs()) {
                            if (p.getPlayer1() == Player.playerPause) {
                                opponent = p.getPlayer2();
                                r.getPairs().remove(p);
                                break;
                            }
                            if (p.getPlayer2() == Player.playerPause) {
                                opponent = p.getPlayer1();
                                r.getPairs().remove(p);
                                break;
                            }
                        }
                        
                        r.addPair(player, opponent);
                    }
                }

                tpane.refreshAllTables();
                tpane.refreshStatus(tpane.getLastPane());
                int rowId = tablePlayers.convertRowIndexToView(t.getPlayerList().size()-1);
                tablePlayers.setRowSelectionInterval(rowId, rowId);

            } catch (SwissIllegalPlayerName se) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("ERR_ILLEGAL_PLAYER_MSG"),
                    _("ERR_ILLEGAL_PLAYER_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
            } catch (SwissPlayerNameCollision se) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("ERR_PLAYER_DUP_MSG"),
                    _("ERR_PLAYER_DUP_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
            } catch (SwissTournamentFinished se) {
            }
        }

        if (event.getSource() == buttonDel) {

            int ind = tablePlayers.getSelectedRow();
            if (ind == -1)
                return;

            if (t.isFinished()) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("ERR_FINISHED_MSG"),
                    _("ERR_FINISHED_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
                
                return;
            }
            
            Player pl = t.getPlayerList().get(ind);
            Round r = t.currentRound();
            if (r != null) {
                Pair p = r.getPairWithPlayer(pl);
                if (p != null && !p.isSet()) {
                    
                    // does should i replace miss with pause player?
                    int n = JOptionPane.showOptionDialog(
                        OSwiss.getMainForm(),
                        _("ASK_AUTOLOSS_MSG"),
                        _("ASK_AUTOLOSS_TITLE"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,     //do not use a custom Icon
                        MainForm.optionsYesNo,  //the titles of buttons
                        MainForm.optionsYesNo[0]); //default button title

                    if (n == JOptionPane.YES_OPTION) {
                        if (pl == p.getPlayer1())
                            p.setPoints(Result.resultLoss);
                        else
                            p.setPoints(Result.resultWin);
                    }
                }
            }

            try {
                t.delPlayer(ind);           
                tpane.refreshAllTables();
                tpane.refreshStatus(tpane.getLastPane());
            } catch (SwissTournamentFinished ex) { }
        }
    }
    
    
    private JTable createTablePlayers(List<Player> playerList) {
        
        JTable table = new JTable();
        
        tableModel = new PlayerTableModel(tpane, playerList);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setModel(tableModel);
        
        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);

        TableColumn tc = table.getColumnModel().getColumn(0);
        tc.setMaxWidth(35);
        tc.setMinWidth(35);
        
        tc = table.getColumnModel().getColumn(2);
        tc.setPreferredWidth(100);
        tc.setMaxWidth(200);
        
        tc = table.getColumnModel().getColumn(3);
        tc.setPreferredWidth(100);
        tc.setMaxWidth(200);
        
        //for (int i=0; i<table.getColumnModel().getColumnCount(); i++)
            table.getColumnModel().getColumn(0).setResizable(false);

        table.getTableHeader().setReorderingAllowed(false);
        
        table.getColumnModel().getColumn(1).setCellRenderer(new PlayerTableCellRenderer());
        table.getColumnModel().getColumn(2).setCellRenderer(dtcr);
        table.getColumnModel().getColumn(3).setCellRenderer(dtcr);

        // table.setAutoCreateRowSorter(true);
        
        TableRowSorter<PlayerTableModel> sorter 
            = new TableRowSorter<PlayerTableModel>(tableModel);
        sorter.setComparator(1, new PlayerComparator());
        table.setRowSorter(sorter);
        table.getSelectionModel().addListSelectionListener(this);
        return table;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        scrollPanePlayers = new JScrollPane();
        tablePlayers = createTablePlayers(t.getPlayerList());
        panelButtons = new JPanel();

        setLayout(new java.awt.BorderLayout());
        
        scrollPanePlayers.setViewportView(tablePlayers);

        add(scrollPanePlayers, BorderLayout.CENTER);

        // panelButtons.setPreferredSize(new java.awt.Dimension(400, 44));

        buttonOpen = new JButton(_("BUTTON_LOAD"),
                OSwiss.getMainForm().getImageIcon("16_load_player.png"));
        buttonOpen.addActionListener(this);
        buttonPrint = new JButton(_("BUTTON_PRINT"),
                OSwiss.getMainForm().getImageIcon("16_print.png"));
        buttonPrint.addActionListener(this);
        buttonAdd = new JButton(_("BUTTON_ADD"),
                OSwiss.getMainForm().getImageIcon("16_add.png"));
        buttonAdd.addActionListener(this);
        buttonDel = new JButton(_("BUTTON_DELETE"),
                OSwiss.getMainForm().getImageIcon("16_del.png"));
        buttonDel.addActionListener(this);

        javax.swing.GroupLayout panelButtonsLayout = new javax.swing.GroupLayout(panelButtons);
        panelButtons.setLayout(panelButtonsLayout);
        panelButtonsLayout.setHorizontalGroup(
            panelButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonDel, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(buttonAdd, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(buttonPrint, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                    .addComponent(buttonOpen, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelButtonsLayout.setVerticalGroup(
            panelButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonDel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(buttonOpen)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 169, Short.MAX_VALUE)
                .addComponent(buttonPrint)
                .addContainerGap())
        );

        add(panelButtons, java.awt.BorderLayout.LINE_END);
    }// </editor-fold>

    @Override
    public String toString()
    {
        return _("TITLE_REGISTRATION");
    }

    @Override
    public void actionPrint() {
        TablePrinter.printTable(OSwiss.getMainForm().getTabName(tpane), toString(), tablePlayers);
    }

    @Override
    public void setEditable(boolean editable) {
        tableModel.setEditable(editable);
    }

    @Override
    public int getStatus() {
        if (!t.getPlayerList().isEmpty())
            return TableCardPane.STATUS_OK;
        
        if (t.getRoundsCount() > 0)
            return TableCardPane.STATUS_FAIL;
        
        return TableCardPane.STATUS_PROGRESS;
    }

    @Override
    public void refreshStatus() {
    }

    @Override
    public boolean isAddAllowed() {
        if (t.isFinished()) {
            buttonOpen.setEnabled(false);
            buttonAdd.setEnabled(false);
            return false;
        }
        
        buttonOpen.setEnabled(true);
        buttonAdd.setEnabled(true);
        return true;
    }

    @Override
    public boolean isDeleteAllowed() {
        if (t.isFinished()) {
            buttonDel.setEnabled(false);
            return false;
        }
        
        int ind = tablePlayers.getSelectedRow();
        if (ind == -1) {
            buttonDel.setEnabled(false);
            return false;
        }
                
        Player pl = t.getPlayerList().get(ind);
        if (pl.isLeaved()) {
            buttonDel.setEnabled(false);
            return false;
        }
        
        buttonDel.setEnabled(true);
        return true;
    }

    @Override
    public boolean isSetResultAllowed() {
        return false;
    }

    @Override
    public boolean isPrintAllowed() {
        return true;
    }

    @Override
    public void actionAdd() {
        this.actionPerformed(new ActionEvent(buttonAdd, 0, null));
    }

    @Override
    public void actionDelete() {
        this.actionPerformed(new ActionEvent(buttonDel, 0, null));
    }

    @Override
    public void actionSetResult(Result result) {
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) {
        OSwiss.getMainForm().refreshEnableIcons();
    }

    @Override
    public void actionFindPlayer(String playerName) {
        
        int index;
        Player pl;
        boolean found = false;
        
        for (int i=0; i<t.getPlayerList().size(); i++) {
            
            pl = t.getPlayerList().get(i);
            index = pl.getName().indexOf(playerName);
            
            if (index >= 0) {
                int rowId = tablePlayers.convertRowIndexToView(i);
                tablePlayers.setRowSelectionInterval(rowId, rowId);
                found = true;
                break;
            }
        }
        
        if (!found)
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("INFO_PLAYER_NOT_FOUND_MSG"),
                _("INFO_PLAYER_NOT_FOUND_TITLE"),
                JOptionPane.INFORMATION_MESSAGE);
    }

}
