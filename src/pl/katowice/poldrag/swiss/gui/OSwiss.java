package pl.katowice.poldrag.swiss.gui;

import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.languages.Languages;



public class OSwiss {

    private static MainForm mainForm;

    public static MainForm getMainForm() {
        return mainForm;
    }

    public static void main(String[] args)
    {
        OptionsDialog.loadPreferences();
        Languages.initMessages();
        Player.initStaticPlayers();
        mainForm = new MainForm();
    }
}
