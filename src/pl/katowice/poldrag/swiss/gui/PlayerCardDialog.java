/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import pl.katowice.poldrag.swiss.core.Pair;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.gui.tablemodels.ColouredIntegerRenderer;
import pl.katowice.poldrag.swiss.gui.tablemodels.PlayerCardTableModel;
import static pl.katowice.poldrag.swiss.languages.Languages._;

/*
 * PlayerCardDialog.java
 *
 * Created on 2012-11-26, 20:28:20
 */

/**
 *
 * @author Buizel
 */
public class PlayerCardDialog extends JDialog
implements ActionListener {

    // Variables declaration - do not modify
    private JButton buttonOk;
    private JButton buttonPrint;
    private JScrollPane jScrollPane1;
    private JLabel labelName;
    private JLabel labelNameVal;
    private JTable tablePlayerCard;
    private PlayerCardTableModel tableModel;
    // End of variables declaration

    private Player player;
    private List<Pair> pairs;

    /** Creates new form PlayerCardDialog */
    protected PlayerCardDialog(Player player, List<Pair> pairs) {
        super(OSwiss.getMainForm(), true);
        this.player = player;
        this.pairs = pairs;
        initComponents();
        this.setTitle(_("DIALOG_CARD_TITLE"));
        this.setMinimumSize(new Dimension(420, 220));
        this.setLocationRelativeTo(OSwiss.getMainForm());
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private JTable createTablePairs(Player player, List<Pair> pairList) {

        JTable table = new JTable();

        tableModel = new PlayerCardTableModel(player, pairList);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.setModel(tableModel);

        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);

        TableColumn tc = table.getColumnModel().getColumn(0);
        tc.setMaxWidth(35);
        tc.setMinWidth(35);

        tc = table.getColumnModel().getColumn(3);
        tc.setPreferredWidth(100);
        tc.setMaxWidth(200);

        tc = table.getColumnModel().getColumn(4);
        tc.setPreferredWidth(50);
        tc.setMaxWidth(100);

        table.getColumnModel().getColumn(0).setResizable(false);
        table.getTableHeader().setReorderingAllowed(false);

        table.getColumnModel().getColumn(1).setCellRenderer(dtcr);
        table.getColumnModel().getColumn(2).setCellRenderer(dtcr);
        table.getColumnModel().getColumn(3).setCellRenderer(dtcr);
        table.getColumnModel().getColumn(4).setCellRenderer(new ColouredIntegerRenderer());

        return table;
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        jScrollPane1 = new JScrollPane();
        tablePlayerCard = createTablePairs(player, pairs);
        buttonOk = new JButton();
        labelName = new JLabel();
        labelNameVal = new JLabel();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        jScrollPane1.setViewportView(tablePlayerCard);

        buttonOk.setText(_("BUTTON_CLOSE"));
        buttonOk.addActionListener(this);
        buttonPrint = new JButton(_("BUTTON_PRINT"),
                OSwiss.getMainForm().getImageIcon("16_print.png"));
        buttonPrint.addActionListener(this);
        labelName.setText(_("DIALOG_PLAYER_NAME"));

        labelNameVal.setForeground(new java.awt.Color(0, 51, 153));
        labelNameVal.setText(player.getName());

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 319, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonPrint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 217, Short.MAX_VALUE)
                        .addComponent(buttonOk))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(labelName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelNameVal)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelName)
                    .addComponent(labelNameVal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonOk)
                    .addComponent(buttonPrint))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>


    public static void showPlayerCard(Player player, List<Pair> pairs) {
        if (player == null)
            return;

        PlayerCardDialog pcd = new PlayerCardDialog(player, pairs);
        pcd.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == buttonOk)
            dispose();

        if (ae.getSource() == buttonPrint)
            TablePrinter.printTable(player.getName(), _("TITLE_PLAYER_CARD"), tablePlayerCard);
    }

}
