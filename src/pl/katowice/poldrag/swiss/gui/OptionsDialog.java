/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Locale;
import java.util.prefs.Preferences;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import pl.katowice.poldrag.swiss.core.AbstractTournament;
import pl.katowice.poldrag.swiss.core.KValues;
import pl.katowice.poldrag.swiss.core.Result;
import pl.katowice.poldrag.swiss.core.Swiss;
import pl.katowice.poldrag.swiss.gui.tablemodels.BorderedTableCellRenderer;
import pl.katowice.poldrag.swiss.gui.tablemodels.OptionsTableModel;
import pl.katowice.poldrag.swiss.gui.tablemodels.PositiveInteger;
import pl.katowice.poldrag.swiss.gui.tablemodels.RankingTableModel;
import pl.katowice.poldrag.swiss.languages.Languages;
import static pl.katowice.poldrag.swiss.languages.Languages._;

/*
 * OptionsDialog.java
 *
 * Created on 2012-11-20, 18:45:02
 */

/**
 *
 * @author Buizel
 */
public class OptionsDialog extends JDialog
implements ActionListener {

    // Options Dialog
    private JButton buttonCancel;
    private JButton buttonSave;
    private JButton buttonDefault;
    private JTabbedPane tabbedPane;

    // Options - general
    private JCheckBox checkBoxAutoRanking;
    private JCheckBox checkBoxAutoTournament;
    private JComboBox comboBoxLanguage;
    private JComboBox comboBoxPairing;
    private JLabel labelLanguage;
    private JLabel labelPairing;
    private JCheckBox checkBoxFirstRound;
    private JLabel labelTop;
    private JCheckBox checkBoxTopDropPlayers;
    private JCheckBox checkBoxTopSwitchPairs;
    private JCheckBox checkBoxTopPairWeakest;

    // Options - ranking
    private JComboBox comboBoxRanking;
    private JLabel labelRankingAlgorithm;
    private JLabel labelRankingTable;
    private JLabel labelRankingValue;
    private JScrollPane scrollPaneTableRanking;
    private JTable tableRanking;
    private JSpinner spinnerRanking;
    private JButton buttonAddRanking;
    private JButton buttonDelRanking;
    private RankingTableModel rankingTableModel;
    private KValues kValues;

    // Options - points
    private JComboBox comboBoxPoints;
    private JLabel labelPoints;
    private JLabel labelTable;
    private JScrollPane scrollPaneTablePoints;
    private JTable tablePoints;
    private JCheckBox checkBoxPause;
    private OptionsTableModel pointsTableModel;

    // Options - directories
    private JButton buttonDirPlayer;
    private JButton buttonDirTournament;
    private JLabel labelDirPlayer;
    private JLabel labelDirTournament;
    private JTextField textFieldDirPlayer;
    private JTextField textFieldDirTournament;

    // preferences variables
    private static final String NODE_NAME = "/pl/katowice/poldrag/swiss";
    private static final Preferences prefs = Preferences.userRoot().node(NODE_NAME);

    private static final String PAIRING_ALGORITHM = "pairingAlgorithm";
    private static final String RANKING_ALGORITHM = "rankingAlgorithm";
    private static final String POINTS_ALGORITHM = "pointsAlgorithm";
    private static final String AUTOSAVE_TOURNAMENT = "autosaveTournament";
    private static final String AUTOSAVE_RANKING = "autosaveRanking";
    private static final String LOCALE = "locale";
    private static final String RESULT_WIN = "resultWin";
    private static final String RESULT_LOSS = "resultLoss";
    private static final String RESULT_DRAW = "resultDraw";
    private static final String RESULT_PAUSE = "resultPause";
    private static final String RESULT_MISS = "resultMiss";
    private static final String DIR_TOURNAMENT = "dirTournament";
    private static final String DIR_PLAYER = "dirPlayer";
    private static final String PAUSE_TYPE = "pauseType";
    private static final String FIRST_ROUND_RANDOM = "firstRound";
    private static final String TOP_PAIR_WEAKEST = "topPairWeakest";
    private static final String TOP_SWITCH_PAIRS = "topSwitchPairs";
    private static final String TOP_DROP_PLAYERS = "topDropPlayers";
    private static final String RANKING_KVALUES = "rankingKValues";
    private static final String RANKING_INITIAL = "rankingInitial";

    private static final int PAIRING_ALGORITHM_DEFAULT = 0;
    private static final int RANKING_ALGORITHM_DEFAULT = 0;
    private static final int POINTS_ALGORITHM_DEFAULT = 0;
    private static final boolean AUTOSAVE_TOURNAMENT_DEFAULT = false;
    private static final boolean AUTOSAVE_RANKING_DEFAULT = false;
    private static final String LOCALE_DEFAULT = "";
    private static final String RESULT_WIN_DEFAULT = "(2 - 0)";
    private static final String RESULT_LOSS_DEFAULT = "(0 - 2)";
    private static final String RESULT_DRAW_DEFAULT = "(1 - 1)";
    private static final String RESULT_PAUSE_DEFAULT = "(2 - 0)";
    private static final String RESULT_MISS_DEFAULT = "(0 - 0)";
    private static final String DIR_TOURNAMENT_DEFAULT = "";
    private static final String DIR_PLAYER_DEFAULT = "";
    private static final boolean PAUSE_TYPE_DEFAULT = false;
    private static final boolean FIRST_ROUND_RANDOM_DEFAULT = false;
    private static final boolean TOP_PAIR_WEAKEST_DEFAULT = true;
    private static final boolean TOP_SWITCH_PAIRS_DEFAULT = true;
    private static final boolean TOP_DROP_PLAYERS_DEFAULT = false;
    private static final String RANKING_KVALUES_DEFAULT = "(1,16)(32,32)";
    private static final int RANKING_INITIAL_DEFAULT = 1600;

    private Result resultList[] = {
        Result.resultWin,
        Result.resultLoss,
        Result.resultDraw,
        Result.resultPause,
        Result.resultMiss
    };

    private JFileChooser directoryChooser;

    private static boolean labelLoaded = false;
    private static String pairingAlgirithmList[];
    private static String rankingAlgirithmList[];
    private static String pointsAlgirithmList[];
    private static String languagesList[];
    private static String languagesIdList[];

    private static String languageName;

    /** Creates new form OptionsDialog */
    private OptionsDialog(java.awt.Frame parent) {
        super(parent, true);

        if (!labelLoaded)
            loadLabels();

        initComponents();
        configurationToForm();
        this.setTitle(_("OPTIONS_TITLE"));
        this.setMinimumSize(new Dimension(450, 400));
        this.setLocationRelativeTo(parent);
    }

    private void loadLabels() {
        pairingAlgirithmList = new String[] {
            _("OPTIONS_SWISS_1_ALG"),
            _("OPTIONS_SWISS_2_ALG")};
        rankingAlgirithmList = new String[] {
            _("OPTIONS_RANKING_1_ALG"),
            _("OPTIONS_RANKING_2_ALG")};
        pointsAlgirithmList = new String[] {
            _("OPTIONS_POINTS_1_ALG"),
            _("OPTIONS_POINTS_2_ALG"),
            _("OPTIONS_POINTS_3_ALG"),
            _("OPTIONS_POINTS_4_ALG"),
            _("OPTIONS_POINTS_5_ALG")};
        languagesList = _("LANGUAGE_LIST").split(",");
        languagesIdList = _("LOCALE_LIST").split(",");
        labelLoaded = true;
    }

    public static void savePreferences() {
        prefs.putInt(PAIRING_ALGORITHM, Swiss.confPairingAlgorithm);
        prefs.putInt(RANKING_ALGORITHM, AbstractTournament.confRankingAlgorithm);
        prefs.putInt(POINTS_ALGORITHM, Swiss.confPointsAlgorithm);
        prefs.putBoolean(AUTOSAVE_TOURNAMENT, TournamentPane.confAutosaveTournament);
        prefs.putBoolean(AUTOSAVE_RANKING, TournamentPane.confAutosaveRanking);
        prefs.put(LOCALE, languageName);
        prefs.put(RESULT_WIN, Result.resultWin.toString());
        prefs.put(RESULT_LOSS, Result.resultLoss.toString());
        prefs.put(RESULT_DRAW, Result.resultDraw.toString());
        prefs.put(RESULT_PAUSE, Result.resultPause.toString());
        prefs.put(RESULT_MISS, Result.resultMiss.toString());
        prefs.put(DIR_TOURNAMENT, TournamentPane.confDirTournament.getPath());
        prefs.put(DIR_PLAYER, MainForm.confDirPlayer.getPath());
        prefs.putBoolean(PAUSE_TYPE, Swiss.confPauseType);
        prefs.putBoolean(FIRST_ROUND_RANDOM, Swiss.confFirstRoundRandom);
        prefs.putBoolean(TOP_PAIR_WEAKEST, Swiss.confTopPairWeakest);
        prefs.putBoolean(TOP_SWITCH_PAIRS, Swiss.confTopSwitchPairs);
        prefs.putBoolean(TOP_DROP_PLAYERS, Swiss.confTopDropPlayers);
        prefs.put(RANKING_KVALUES, AbstractTournament.confRankingKValues.serialize());
        prefs.putInt(RANKING_INITIAL, AbstractTournament.confRankingInitial);
    }

    public static void loadPreferences() {
        Swiss.confPairingAlgorithm = prefs.getInt(PAIRING_ALGORITHM, PAIRING_ALGORITHM_DEFAULT);
        AbstractTournament.confRankingAlgorithm = prefs.getInt(RANKING_ALGORITHM, RANKING_ALGORITHM_DEFAULT);
        Swiss.confPointsAlgorithm = prefs.getInt(POINTS_ALGORITHM, POINTS_ALGORITHM_DEFAULT);
        TournamentPane.confAutosaveTournament = prefs.getBoolean(AUTOSAVE_TOURNAMENT, AUTOSAVE_TOURNAMENT_DEFAULT);
        TournamentPane.confAutosaveRanking = prefs.getBoolean(AUTOSAVE_RANKING, AUTOSAVE_RANKING_DEFAULT);

        languageName = prefs.get(LOCALE, LOCALE_DEFAULT);
        if (languageName.equals(""))
            Languages.confLocale = Locale.getDefault();
        else
            Languages.confLocale = new Locale(languageName);

        Result.resultWin = Result.parseRes(prefs.get(RESULT_WIN, RESULT_WIN_DEFAULT));
        Result.resultLoss = Result.parseRes(prefs.get(RESULT_LOSS, RESULT_LOSS_DEFAULT));
        Result.resultDraw = Result.parseRes(prefs.get(RESULT_DRAW, RESULT_DRAW_DEFAULT));
        Result.resultPause = Result.parseRes(prefs.get(RESULT_PAUSE, RESULT_PAUSE_DEFAULT));
        Result.resultMiss = Result.parseRes(prefs.get(RESULT_MISS, RESULT_MISS_DEFAULT));

        Result.resultRevPause = new Result(Result.resultPause);
        Result.resultRevPause.revert();
        Result.resultRevMiss = new Result(Result.resultMiss);
        Result.resultRevMiss.revert();

        Result.resultMiss = Result.parseRes(prefs.get(RESULT_MISS, RESULT_MISS_DEFAULT));
        TournamentPane.confDirTournament = new File(prefs.get(DIR_TOURNAMENT, DIR_TOURNAMENT_DEFAULT));
        MainForm.confDirPlayer = new File(prefs.get(DIR_PLAYER, DIR_PLAYER_DEFAULT));

        Swiss.confPauseType = prefs.getBoolean(PAUSE_TYPE, PAUSE_TYPE_DEFAULT);
        Swiss.confFirstRoundRandom = prefs.getBoolean(FIRST_ROUND_RANDOM, FIRST_ROUND_RANDOM_DEFAULT);
        Swiss.confTopPairWeakest = prefs.getBoolean(TOP_PAIR_WEAKEST, TOP_PAIR_WEAKEST_DEFAULT);
        Swiss.confTopSwitchPairs = prefs.getBoolean(TOP_SWITCH_PAIRS, TOP_SWITCH_PAIRS_DEFAULT);
        Swiss.confTopDropPlayers = prefs.getBoolean(TOP_DROP_PLAYERS, TOP_DROP_PLAYERS_DEFAULT);
        AbstractTournament.confRankingKValues = KValues.unserialize(prefs.get(RANKING_KVALUES, RANKING_KVALUES_DEFAULT));
        AbstractTournament.confRankingInitial = prefs.getInt(RANKING_INITIAL, RANKING_INITIAL_DEFAULT);
    }


    private JTable createRankingTable(KValues kValues) {
	    
        JTable table = new JTable();
        rankingTableModel = new RankingTableModel(kValues);
        table.setModel(rankingTableModel);

        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        table.setDefaultRenderer(PositiveInteger.class, dtcr);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        for (int i=0; i<table.getColumnModel().getColumnCount(); i++)
            table.getColumnModel().getColumn(i).setResizable(false);

        table.getTableHeader().setReorderingAllowed(false);

        return table;
    }
    
    private JTable createPointsTable(Result[] resultList) {

        JTable table = new JTable();
        pointsTableModel = new OptionsTableModel(resultList);
        table.setModel(pointsTableModel);

        DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
        dtcr.setHorizontalAlignment(SwingConstants.CENTER);
        table.setDefaultRenderer(PositiveInteger.class, dtcr);
        table.getColumnModel().getColumn(0).setCellRenderer(new BorderedTableCellRenderer());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        TableColumn tc = table.getColumnModel().getColumn(0);
        tc.setMaxWidth(100);
        tc.setMinWidth(100);

        for (int i=0; i<table.getColumnModel().getColumnCount(); i++)
            table.getColumnModel().getColumn(i).setResizable(false);

        table.getTableHeader().setReorderingAllowed(false);

        return table;
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initTabRanking(JPanel tabPane) {

        labelRankingAlgorithm = new JLabel(_("OPTIONS_RANKING_ALG"));
        labelRankingTable = new JLabel(_("OPTIONS_RANKING_TABLE"));
        labelRankingValue = new JLabel(_("OPTIONS_RANKING_VALUE"));
        buttonAddRanking = new JButton(_("BUTTON_ADD"));
        buttonDelRanking = new JButton(_("BUTTON_DELETE"));
        kValues = new KValues();

        comboBoxRanking = new JComboBox(new DefaultComboBoxModel(rankingAlgirithmList));
        spinnerRanking = new JSpinner();

        tableRanking = createRankingTable(kValues);
        scrollPaneTableRanking = new JScrollPane();
        scrollPaneTableRanking.setViewportView(tableRanking);

        buttonAddRanking.addActionListener(this);
        buttonDelRanking.addActionListener(this);

        GroupLayout layout = new GroupLayout(tabPane);
        tabPane.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                            .addComponent(labelRankingValue, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(labelRankingAlgorithm, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(spinnerRanking, GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                            .addComponent(comboBoxRanking, 0, 273, Short.MAX_VALUE)))
                    .addComponent(labelRankingTable, GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(scrollPaneTableRanking, GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                            .addComponent(buttonDelRanking, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(buttonAddRanking, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(labelRankingAlgorithm)
                    .addComponent(comboBoxRanking, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(spinnerRanking, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelRankingValue))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(labelRankingTable)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonAddRanking)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonDelRanking))
                    .addComponent(scrollPaneTableRanking, GroupLayout.DEFAULT_SIZE, 152, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initTabDirectories(JPanel tabPane) {

        labelDirTournament = new JLabel();
        textFieldDirTournament = new JTextField();
        buttonDirTournament = new JButton();
        labelDirPlayer = new JLabel();
        textFieldDirPlayer = new JTextField();
        buttonDirPlayer = new JButton();

        labelDirTournament.setText(_("OPTIONS_TOURNAMENT_DIR"));

        textFieldDirTournament.setText("");

        buttonDirTournament.setText(_("OPTIONS_SET_DIR"));
        buttonDirTournament.addActionListener(this);

        labelDirPlayer.setText(_("OPTIONS_PLAYER_DIR"));

        textFieldDirPlayer.setText("");

        buttonDirPlayer.setText(_("OPTIONS_SET_DIR"));
        buttonDirPlayer.addActionListener(this);

        GroupLayout layout = new GroupLayout(tabPane);
        tabPane.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelDirTournament)
                    .addComponent(textFieldDirTournament, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(buttonDirTournament, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(textFieldDirPlayer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(buttonDirPlayer, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(labelDirPlayer))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelDirTournament)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldDirTournament, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonDirTournament)
                .addGap(18, 18, 18)
                .addComponent(labelDirPlayer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textFieldDirPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonDirPlayer)
                .addContainerGap(133, Short.MAX_VALUE))
        );
    }// </editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initTabPoints(JPanel tabPane) {

        labelPoints = new JLabel();
        comboBoxPoints = new JComboBox();
        labelTable = new JLabel();
        scrollPaneTablePoints = new JScrollPane();
        tablePoints = createPointsTable(resultList);
        checkBoxPause = new JCheckBox();
        checkBoxFirstRound = new JCheckBox();

        labelPoints.setText(_("OPTIONS_POINTS_ALG"));
        comboBoxPoints.setModel(new DefaultComboBoxModel(pointsAlgirithmList));

        labelTable.setText(_("OPTIONS_POINTS_CONF"));
        scrollPaneTablePoints.setViewportView(tablePoints);
        checkBoxPause.setText(_("OPTIONS_PAUSE_TYPE"));
        checkBoxFirstRound.setText(_("OPTIONS_FIRST_ROUND"));

        GroupLayout layout = new GroupLayout(tabPane);
        tabPane.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollPaneTablePoints, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelPoints)
                        .addGap(18, 18, 18)
                        .addComponent(comboBoxPoints, 0, 282, Short.MAX_VALUE))
                    .addComponent(labelTable)
                    .addComponent(checkBoxPause)
                    .addComponent(checkBoxFirstRound))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPoints)
                    .addComponent(comboBoxPoints, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(labelTable)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPaneTablePoints, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkBoxPause)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkBoxFirstRound)
                .addContainerGap())
        );
    }// </editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initTabGeneral(JPanel tabPane) {

        labelPairing = new JLabel();
        comboBoxPairing = new JComboBox();
        checkBoxAutoTournament = new JCheckBox();
        checkBoxAutoRanking = new JCheckBox();
        labelLanguage = new JLabel();
        comboBoxLanguage = new JComboBox();
        labelTop = new JLabel();
        checkBoxTopDropPlayers = new JCheckBox();
        checkBoxTopSwitchPairs = new JCheckBox();
        checkBoxTopPairWeakest = new JCheckBox();

        labelPairing.setText(_("OPTIONS_SWISS_ALG"));
        comboBoxPairing.setModel(new DefaultComboBoxModel(pairingAlgirithmList));

        checkBoxAutoTournament.setText(_("OPTIONS_AUTOSAVE_T"));
        checkBoxAutoRanking.setText(_("OPTIONS_AUTOSAVE_R"));
        labelLanguage.setText(_("OPTIONS_LANGUAGE"));
        comboBoxLanguage.setModel(new DefaultComboBoxModel(languagesList));
        
        labelTop.setText(_("OPTIONS_TOP_ROUNDS"));
        checkBoxTopPairWeakest.setText(_("OPTIONS_TOP_BEST_WITH_WEAKEST"));
        checkBoxTopSwitchPairs.setText(_("OPTIONS_TOP_SWITCH_PAIRS"));
        checkBoxTopDropPlayers.setText(_("OPTIONS_TOP_DROP_PLAYERS"));

        GroupLayout layout = new GroupLayout(tabPane);
        tabPane.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(labelPairing)
                    .addComponent(labelLanguage)
                    .addComponent(labelTop))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(checkBoxAutoTournament)
                    .addComponent(checkBoxAutoRanking)
                    .addComponent(comboBoxLanguage, 0, 274, Short.MAX_VALUE)
                    .addComponent(checkBoxTopDropPlayers)
                    .addComponent(checkBoxTopSwitchPairs)
                    .addComponent(checkBoxTopPairWeakest)
                    .addComponent(comboBoxPairing, GroupLayout.Alignment.TRAILING, 0, 274, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPairing)
                    .addComponent(comboBoxPairing, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(checkBoxAutoTournament)
                .addGap(7, 7, 7)
                .addComponent(checkBoxAutoRanking)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(labelLanguage)
                    .addComponent(comboBoxLanguage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(checkBoxTopPairWeakest)
                    .addComponent(labelTop))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(checkBoxTopSwitchPairs)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(checkBoxTopDropPlayers)
                .addContainerGap(72, Short.MAX_VALUE))
        );
    }// </editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        tabbedPane = new JTabbedPane();
        buttonSave = new JButton();
        buttonCancel = new JButton();
        buttonDefault = new JButton();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        buttonSave.setText(_("BUTTON_SAVE"));
        buttonSave.addActionListener(this);

        buttonCancel.setText(_("BUTTON_CANCEL"));
        buttonCancel.addActionListener(this);

        buttonDefault.setText(_("BUTTON_RESTORE_DEFAULT"));
        buttonDefault.addActionListener(this);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonDefault)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 115, Short.MAX_VALUE)
                .addComponent(buttonCancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonSave)
                .addContainerGap())
            .addComponent(tabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(tabbedPane, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonSave)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(buttonCancel)
                        .addComponent(buttonDefault)))
                .addContainerGap())
        );

        JPanel paneGeneral = new JPanel();
        initTabGeneral(paneGeneral);
        tabbedPane.add(_("OPTIONS_TAB_GENERAL"), paneGeneral);

        JPanel paneRanking = new JPanel();
        initTabRanking(paneRanking);
        tabbedPane.add(_("OPTIONS_TAB_RANKING"), paneRanking);

        JPanel panePoints = new JPanel();
        initTabPoints(panePoints);
        tabbedPane.add(_("OPTIONS_TAB_POINTS"), panePoints);

        JPanel paneDirectories = new JPanel();
        initTabDirectories(paneDirectories);
        tabbedPane.add(_("OPTIONS_TAB_DIR"), paneDirectories);

        directoryChooser = new JFileChooser();
        directoryChooser.setDialogTitle(_("OPTIONS_CHOOSE_DIR_TITLE"));
        directoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        directoryChooser.setAcceptAllFileFilterUsed(false);

        pack();
    }// </editor-fold>


    public static void showOptionsDialog() {
        OptionsDialog od = new OptionsDialog(OSwiss.getMainForm());
        od.setVisible(true);
    }


    private void configurationToForm() {
        comboBoxPairing.setSelectedIndex(Swiss.confPairingAlgorithm);
        comboBoxRanking.setSelectedIndex(AbstractTournament.confRankingAlgorithm);
        comboBoxPoints.setSelectedIndex(Swiss.confPointsAlgorithm);
        checkBoxAutoTournament.setSelected(TournamentPane.confAutosaveTournament);
        checkBoxAutoRanking.setSelected(TournamentPane.confAutosaveRanking);

        int ind = 0;
        for(int i=0; i<languagesIdList.length; i++) {
            if (languagesIdList[i].equals(languageName)) {
                ind = i;
                break;
            }
        }
        comboBoxLanguage.setSelectedIndex(ind);

        resultList[0] = Result.resultWin;
        resultList[1] = Result.resultLoss;
        resultList[2] = Result.resultDraw;
        resultList[3] = Result.resultPause;
        resultList[4] = Result.resultMiss;
        pointsTableModel.fireTableDataChanged();

        textFieldDirTournament.setText(TournamentPane.confDirTournament.getPath());
        textFieldDirPlayer.setText(MainForm.confDirPlayer.getPath());

        checkBoxPause.setSelected(Swiss.confPauseType);
        checkBoxFirstRound.setSelected(Swiss.confFirstRoundRandom);
        checkBoxTopPairWeakest.setSelected(Swiss.confTopPairWeakest);
        checkBoxTopSwitchPairs.setSelected(Swiss.confTopSwitchPairs);
        checkBoxTopDropPlayers.setSelected(Swiss.confTopDropPlayers);
        spinnerRanking.setValue(AbstractTournament.confRankingInitial);
        AbstractTournament.confRankingKValues.copy(kValues);
        rankingTableModel.fireTableDataChanged();
    }


    private void defaultToForm() {
        comboBoxPairing.setSelectedIndex(PAIRING_ALGORITHM_DEFAULT);
        comboBoxRanking.setSelectedIndex(RANKING_ALGORITHM_DEFAULT);
        comboBoxPoints.setSelectedIndex(POINTS_ALGORITHM_DEFAULT);
        checkBoxAutoTournament.setSelected(AUTOSAVE_TOURNAMENT_DEFAULT);
        checkBoxAutoRanking.setSelected(AUTOSAVE_RANKING_DEFAULT);

        int ind = 0;
        for(int i=0; i<languagesIdList.length; i++) {
            if (languagesIdList[i].equals(LOCALE_DEFAULT)) {
                ind = i;
                break;
            }
        }
        comboBoxLanguage.setSelectedIndex(ind);

        resultList[0] = Result.parseRes(RESULT_WIN_DEFAULT);
        resultList[1] = Result.parseRes(RESULT_LOSS_DEFAULT);
        resultList[2] = Result.parseRes(RESULT_DRAW_DEFAULT);
        resultList[3] = Result.parseRes(RESULT_PAUSE_DEFAULT);
        resultList[4] = Result.parseRes(RESULT_MISS_DEFAULT);
        pointsTableModel.fireTableDataChanged();

        textFieldDirTournament.setText(DIR_TOURNAMENT_DEFAULT);
        textFieldDirPlayer.setText(DIR_PLAYER_DEFAULT);

        checkBoxPause.setSelected(PAUSE_TYPE_DEFAULT);
        checkBoxFirstRound.setSelected(FIRST_ROUND_RANDOM_DEFAULT);
        checkBoxTopPairWeakest.setSelected(TOP_PAIR_WEAKEST_DEFAULT);
        checkBoxTopSwitchPairs.setSelected(TOP_SWITCH_PAIRS_DEFAULT);
        checkBoxTopDropPlayers.setSelected(TOP_DROP_PLAYERS_DEFAULT);
        spinnerRanking.setValue(RANKING_INITIAL_DEFAULT);
        KValues.unserialize(RANKING_KVALUES_DEFAULT).copy(kValues);
        rankingTableModel.fireTableDataChanged();
    }


    private void formToConfiguration() {
        Swiss.confPairingAlgorithm = comboBoxPairing.getSelectedIndex();
        AbstractTournament.confRankingAlgorithm = comboBoxRanking.getSelectedIndex();
        Swiss.confPointsAlgorithm = comboBoxPoints.getSelectedIndex();
        TournamentPane.confAutosaveTournament = checkBoxAutoTournament.isSelected();
        TournamentPane.confAutosaveRanking = checkBoxAutoRanking.isSelected();
        languageName = languagesIdList[comboBoxLanguage.getSelectedIndex()];
        Languages.confLocale = new Locale(languageName);

        Result.resultWin = resultList[0];
        Result.resultLoss = resultList[1];
        Result.resultDraw = resultList[2];
        Result.resultPause = resultList[3];
        Result.resultMiss = resultList[4];

        Result.resultRevPause = new Result(Result.resultPause);
        Result.resultRevPause.revert();
        Result.resultRevMiss = new Result(Result.resultMiss);
        Result.resultRevMiss.revert();

        TournamentPane.confDirTournament = new File(textFieldDirTournament.getText());
        MainForm.confDirPlayer = new File(textFieldDirPlayer.getText());

        Swiss.confPauseType = checkBoxPause.isSelected();
        Swiss.confFirstRoundRandom = checkBoxFirstRound.isSelected();
        Swiss.confTopPairWeakest = checkBoxTopPairWeakest.isSelected();
        Swiss.confTopSwitchPairs = checkBoxTopSwitchPairs.isSelected();
        Swiss.confTopDropPlayers = checkBoxTopDropPlayers.isSelected();
        AbstractTournament.confRankingInitial = (Integer)spinnerRanking.getValue();
        AbstractTournament.confRankingKValues = kValues.clone();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == buttonSave) {
            boolean changedLanguage =
                    !languageName.equals(languagesIdList[comboBoxLanguage.getSelectedIndex()]);

            formToConfiguration();
            savePreferences();

            if (changedLanguage) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                       _("INFO_OPTIONS_RESTART_MSG"),
                       _("INFO_OPTIONS_RESTART_TITLE"),
                       JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("INFO_OPTIONS_SAVED_MSG"),
                    _("INFO_OPTIONS_SAVED_TITLE"),
                    JOptionPane.INFORMATION_MESSAGE);
            }
        }
	
        if (ae.getSource() == buttonAddRanking) {
            KValues.Entry kValuesEntry;
            if ((kValuesEntry = AddKValueDialog.showAddKValueDialog()) != null) {
                kValues.put(kValuesEntry);
                rankingTableModel.fireTableDataChanged();
            }
        }

        if (ae.getSource() == buttonDelRanking) {
            int ind = tableRanking.getSelectedRow();
            if (ind == -1)
                return;
            
            kValues.removePosition(tableRanking.convertRowIndexToModel(ind));
            rankingTableModel.fireTableDataChanged();
        }

        if (ae.getSource() == buttonDefault) {
            defaultToForm();
        }


        if (ae.getSource() == buttonCancel) {
            dispose();
        }

        if (ae.getSource() == buttonDirPlayer) {
            directoryChooser.setCurrentDirectory(MainForm.confDirPlayer);
            if (directoryChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

                File file = directoryChooser.getSelectedFile();
                if (file.exists()) {
                    MainForm.confDirPlayer = file;
                    textFieldDirPlayer.setText(MainForm.confDirPlayer.getPath());
                }
            }
        }

        if (ae.getSource() == buttonDirTournament) {
            directoryChooser.setCurrentDirectory(TournamentPane.confDirTournament);
            if (directoryChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

                File file = directoryChooser.getSelectedFile();
                if (file.exists()) {
                    TournamentPane.confDirTournament = file;
                    textFieldDirTournament.setText(TournamentPane.confDirTournament.getPath());
                }
            }
        }

    }
}
