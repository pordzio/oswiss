/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import static pl.katowice.poldrag.swiss.languages.Languages._;


/**
 *
 * @author kkopec
 */
class StartTopDialog extends JDialog
implements ActionListener {

    private JButton buttonCancel;
    private JButton buttonOK;
    private JComboBox comboBoxTop;
    private JLabel labelTop;

    private int result;

    /** Creates new form AddPairDialog */
    private StartTopDialog(Frame parent, int maxTop) {
        super(parent, true);
        initComponents(maxTop);
        this.setTitle(_("DIALOG_TOP_TITLE"));
        this.setResizable(false);
        this.setLocationRelativeTo(parent);
    }

    private void initComponents(int maxTop) {

        labelTop = new JLabel();
        comboBoxTop = new JComboBox();
        buttonOK = new JButton();
        buttonCancel = new JButton();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        labelTop.setText(_("DIALOG_TOP_NUM_PLAYERS"));

        ArrayList<Integer> comboBoxOptions = new ArrayList<Integer>();
        int top = 2;
        while (top <= maxTop) {
            comboBoxOptions.add(top);
            top *= 2;
        }
        comboBoxTop.setModel(new DefaultComboBoxModel(comboBoxOptions.toArray()));

        buttonOK.setText(_("BUTTON_OK"));
        buttonCancel.setText(_("BUTTON_CANCEL"));

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelTop)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(comboBoxTop, 0, 176, Short.MAX_VALUE))
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(buttonCancel)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonOK)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(labelTop)
                    .addComponent(comboBoxTop, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(buttonOK)
                    .addComponent(buttonCancel))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonOK.addActionListener(this);
        buttonCancel.addActionListener(this);

        pack();
    }

    static int showDialog(int maxTop) {
        StartTopDialog dialog = new StartTopDialog(OSwiss.getMainForm(), maxTop);
        dialog.setVisible(true);
        return dialog.result;
    }

    private int indexToResult(int index) {
        int res = 2;
        for (int i=0; i<index; i++)
            res *= 2;
        return res;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == buttonOK) {
            result = indexToResult(comboBoxTop.getSelectedIndex());
            dispose();
        }
        if (e.getSource() == buttonCancel) {
            dispose();
        }
    }

}
