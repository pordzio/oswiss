package pl.katowice.poldrag.swiss.gui;

import com.thoughtworks.xstream.io.StreamException;
import java.awt.CardLayout;
import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import pl.katowice.poldrag.swiss.core.Round;
import pl.katowice.poldrag.swiss.core.Tournament;
import pl.katowice.poldrag.swiss.exceptions.*;
import pl.katowice.poldrag.swiss.gui.tablemodels.IconListRenderer;
import pl.katowice.poldrag.swiss.gui.tablemodels.RoundListModel;
import static pl.katowice.poldrag.swiss.languages.Languages._;

public class TournamentPane extends JSplitPane
implements ListSelectionListener
{
    private JPanel panelCard;
    private JScrollPane scrollPaneRound;
    private JList list;
    private RoundListModel listModel;
    private File tournamentFile;

    private Tournament t;

    public static boolean confAutosaveTournament;
    public static boolean confAutosaveRanking;
    public static File confDirTournament;

    protected static File localPathTournament = null;

    public TournamentPane(Tournament t) {
        this.t = t;
        initComponents();
    }

    // open tournament from external file file
    public TournamentPane()
    throws StreamException, FileNotFoundException {
        JFileChooser fileChooserTournament =
                OSwiss.getMainForm().getFileChooserTournament();

        if (localPathTournament == null)
            localPathTournament = confDirTournament;

        fileChooserTournament.setCurrentDirectory(localPathTournament);

        int ret = fileChooserTournament.showOpenDialog(OSwiss.getMainForm());

        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileChooserTournament.getSelectedFile();
            this.t = FileManager.openTournament(file);
            tournamentFile = file;
            localPathTournament = new File(file.getPath());

        } else
            throw new FileNotFoundException();

        initComponents();
    }

    public File getTournamentFile() {
        return tournamentFile;
    }

    public Tournament getTournament() {
        return t;
    }

    public void setTournamentFile(File tournamentFile) {
        this.tournamentFile = tournamentFile;
    }

    public void refreshAllTables()
    {
        for (int i=0; i<listModel.size(); i++) {
            ((TableCardPane)listModel.get(i)).refreshTable();
        }
    }

    public void printPlayerList()
    {
        if (listModel.size() == 0)
            return;

        ((TableCardPane)listModel.get(0)).actionPrint();
    }

    public void refreshStatus(TableCardPane invoker) {
        if (invoker != null) {
            invoker.refreshStatus();
            listModel.fireContentsChanged(listModel.indexOf(invoker));
        } else
            listModel.fireContentsChanged();
    }

    TableCardPane getLastPane() {
        if (listModel.size() == 0)
            return null;

        return (TableCardPane)listModel.get(listModel.size()-1);
    }

    TableCardPane getCurrentPane() {
        if (listModel.size() == 0)
            return null;

        TableCardPane card = null;

        for (Component comp : panelCard.getComponents()) {
            if (comp.isVisible() == true) {
                card = (TableCardPane)comp;
                break;
            }
        }

        return card;
    }

    public void setEditableAllTables(boolean editable)
    {
        for (int i=0; i<listModel.size(); i++) {
            ((TableCardPane)listModel.get(i)).setEditable(editable);
        }
    }

    void saveTournament() {

        File file = getTournamentFile();
        if (file == null) {
            saveTournamentAs();
            return;
        }

        try {
            FileManager.saveTournament(t, file);
            setTournamentFile(file);
            OSwiss.getMainForm().setTabName(this, file.getName());

            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("INFO_TOURNAMENT_SAVED_MSG"),
                    _("INFO_TOURNAMENT_SAVED_TITLE"),
                    JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_SAVE_MSG"),
                _("ERR_SAVE_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        }
    }

    void saveTournamentAs() {
        JFileChooser fileChooserTournament =
                OSwiss.getMainForm().getFileChooserTournament();

        if (tournamentFile != null) {
            fileChooserTournament.setSelectedFile(tournamentFile);
        } else {
            File file = FileManager.addExtensionIfNeeded(
                    new File(OSwiss.getMainForm().getTabName(this)), FileManager.EXT_TOURNAMENT);
            fileChooserTournament.setSelectedFile(file);
        }

        if (localPathTournament == null)
            localPathTournament = confDirTournament;

        fileChooserTournament.setCurrentDirectory(localPathTournament);

        int ret = fileChooserTournament.showSaveDialog(OSwiss.getMainForm());

        if (ret == JFileChooser.APPROVE_OPTION) {
            try {
                File file = FileManager.addExtensionIfNeeded(
                        fileChooserTournament.getSelectedFile(), FileManager.EXT_TOURNAMENT);

                int n = JOptionPane.YES_OPTION;
                if (file.exists())
                    n = JOptionPane.showOptionDialog(
                        OSwiss.getMainForm(),
                        _("ASK_REPLACE_MSG"),
                        _("ASK_REPLACE_TITLE"),
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,     //do not use a custom Icon
                        MainForm.optionsYesNo,  //the titles of buttons
                        MainForm.optionsYesNo[0]); //default button title

                if (n == JOptionPane.YES_OPTION) {
                    FileManager.saveTournament(t, file);
                    setTournamentFile(file);
                    localPathTournament = new File(file.getPath());
                    OSwiss.getMainForm().setTabName(this, file.getName());
                }

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("ERR_SAVE_MSG"),
                    _("ERR_SAVE_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    boolean closeTournament() {
        int n = JOptionPane.showOptionDialog(
            OSwiss.getMainForm(),
            _("ASK_CLOSE_TOURNAMENT_MSG"),
            _("ASK_CLOSE_TOURNAMENT_TITLE"),
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,
            null,     //do not use a custom Icon
            MainForm.optionsYesNo,  //the titles of buttons
            MainForm.optionsYesNo[0]); //default button title

        return (n == JOptionPane.YES_OPTION);
    }


    protected void autosaveTournament() {
        if (confAutosaveTournament && (tournamentFile == null || !tournamentFile.exists())) {

            int n = JOptionPane.showOptionDialog(
                OSwiss.getMainForm(),
                _("ASK_AUTOSAVE_TOURNAMENT_MSG"),
                _("ASK_AUTOSAVE_TOURNAMENT_TITLE"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,     //do not use a custom Icon
                MainForm.optionsYesNo,  //the titles of buttons
                MainForm.optionsYesNo[0]); //default button title

            if (n == JOptionPane.YES_OPTION)
                saveTournamentAs();
        } else
        if (confAutosaveTournament && tournamentFile != null && tournamentFile.exists()) {
            try {
                FileManager.saveTournament(t, tournamentFile);
            } catch (IOException ex) { }
        }
    }

    public void startNewRound()
    {
        try {
            JPanel roundPane;
            try {
                roundPane = new PairListPane(this, t.startNewRound());
            } catch (SwissPairingNotPossible se) {
                int n = JOptionPane.showOptionDialog(
                OSwiss.getMainForm(),
                    _("ASK_ADD_EMPTY_ROUND_MSG"),
                    _("ASK_ADD_EMPTY_ROUND_TITLE"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,     //do not use a custom Icon
                    MainForm.optionsYesNo,  //the titles of buttons
                    MainForm.optionsYesNo[0]); //default button title

                if (n == JOptionPane.YES_OPTION) {
                    roundPane = new PairListPane(this, t.startEmptyRound());
                } else
                    throw new SwissPairingNotPossible();
            }
                
            addRound(roundPane);
            ((TableCardPane)listModel.get(0)).refreshTable();
            list.setSelectedIndex(listModel.size() - 1);
            autosaveTournament();

        } catch (SwissEmptyPlayerList se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_PLAYERS_MSG"),
                _("ERR_NO_PLAYERS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissPairingNotPossible se) {
        } catch (SwissPairingTopNotPossible se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_PAIRINGS_MSG"),
                _("ERR_NO_PAIRINGS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissResultsNotFilled se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NOT_FILLED_MSG"),
                _("ERR_NOT_FILLED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissIllegalPairs se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_ILLEGAL_PAIRS_MSG"),
                _("ERR_ILLEGAL_PAIRS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissTournamentFinished se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_FINISHED_MSG"),
                _("ERR_FINISHED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissException se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_UNKNOWN_MSG"),
                _("ERR_UNKNOWN_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        }
    }

    void startTopRound() {

        int maxTop = 2;
        int plyersCount = t.getPlayerList().size();

        while (maxTop <= plyersCount)
            maxTop *= 2;
        maxTop /= 2;

        try {
            // if no players then show error
            if (plyersCount == 0) {
                throw new SwissEmptyPlayerList();
            }

            if (maxTop <= 1)
                throw new SwissPairingNotPossible();

            // if tops already started, then do nothing
            Round currentRound = t.currentRound();
            if (currentRound!=null && currentRound.isTop())
                return;

            int top = StartTopDialog.showDialog(maxTop);

            // aborted by user
            if (top == 0)
                return;

            // start new top
            JPanel roundPane = new PairListPane(this, t.startTopRound(top));
            addRound(roundPane);
            ((TableCardPane)listModel.get(0)).refreshTable();
            list.setSelectedIndex(listModel.size() - 1);
            autosaveTournament();
        } catch (SwissEmptyPlayerList se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_PLAYERS_MSG"),
                _("ERR_NO_PLAYERS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissPairingNotPossible se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_PAIRINGS_MSG"),
                _("ERR_NO_PAIRINGS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissResultsNotFilled se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NOT_FILLED_MSG"),
                _("ERR_NOT_FILLED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissIllegalPairs se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_ILLEGAL_PAIRS_MSG"),
                _("ERR_ILLEGAL_PAIRS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissTournamentFinished se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_FINISHED_MSG"),
                _("ERR_FINISHED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissException se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_UNKNOWN_MSG"),
                _("ERR_UNKNOWN_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        }

    }

    void endTournament() {

        try {

            t.finishTournament();
            StandingsPane spane = new StandingsPane(this, t);
            addRound(spane);
            setEditableAllTables(false);
            autosaveTournament();

            if (confAutosaveRanking)
                spane.savePlayerRanking();

        } catch (SwissResultsNotFilled se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NOT_FILLED_MSG"),
                _("ERR_NOT_FILLED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissIllegalPairs se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_ILLEGAL_PAIRS_MSG"),
                _("ERR_ILLEGAL_PAIRS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissEmptyPlayerList se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_PLAYERS_MSG"),
                _("ERR_NO_PLAYERS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissTopRoundMissing se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_TOP_ROUND_MSG"),
                _("ERR_NO_TOP_ROUND_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissNoRounds se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_ROUNDS_MSG"),
                _("ERR_NO_ROUNDS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissTournamentFinished se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_FINISHED_MSG"),
                _("ERR_FINISHED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        }
    }


    void resumeTournament() {
        try {

            t.resumeTournament();
            JPanel StandingsPane = (JPanel)listModel.get(listModel.size() - 1);
            delRound(StandingsPane);
            setEditableAllTables(true);
            list.setSelectedIndex(listModel.size() - 1);

        } catch (SwissTournamentFinished ex) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NOT_FINISHED_MSG"),
                _("ERR_NOT_FINISHED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deleteLastRound()
    {
        try {
            t.deleteLastRound();
            JPanel roundPane = (JPanel)listModel.get(listModel.size() - 1);
            delRound(roundPane);
            list.setSelectedIndex(listModel.size() - 1);
        } catch (SwissTournamentFinished se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_FINISHED_MSG"),
                _("ERR_FINISHED_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        } catch (SwissNoRounds se) {
            JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                _("ERR_NO_ROUNDS_MSG"),
                _("ERR_NO_ROUNDS_TITLE"),
                JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e)
    {
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        int minIndex = lsm.getMinSelectionIndex();

        if (! lsm.isSelectionEmpty()) {
            CardLayout cl = (CardLayout)(panelCard.getLayout());
            cl.show(panelCard, listModel.get(minIndex).toString());
            OSwiss.getMainForm().refreshEnableIcons();
        }
    }

    private void delRound(JPanel pane)
    {
        listModel.removeElement(pane);
        panelCard.remove(pane);
    }

    private void addRound(JPanel pane)
    {
        listModel.addElement(pane);
        panelCard.add(pane, pane.toString());
    }

    @SuppressWarnings("unchecked")
    private void initComponents()
    {
        scrollPaneRound = new JScrollPane();
        list = new JList();
        panelCard = new JPanel();

        scrollPaneRound.setMinimumSize(new java.awt.Dimension(150, 23));

        list = new JList();
        ListSelectionModel listSelectionModel = list.getSelectionModel();
        listSelectionModel.addListSelectionListener(this);
        list.setCellRenderer(new IconListRenderer());

        list.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        list.setMinimumSize(new java.awt.Dimension(150, 80));
        scrollPaneRound.setViewportView(list);

        setLeftComponent(scrollPaneRound);

        panelCard.setLayout(new java.awt.CardLayout());
        setRightComponent(panelCard);

        listModel = new RoundListModel();

        addRound(new PlayerListPane(this, t));
        for (int i=0; i<t.getRoundsCount(); i++)
            addRound(new PairListPane(this, t.getRound(i)));

        if (t.isFinished()) {
            addRound(new StandingsPane(this, t));
            setEditableAllTables(false);
        }

        list.setModel(listModel);
        list.setSelectedIndex(0);

        setVisible(true);
    }

}
