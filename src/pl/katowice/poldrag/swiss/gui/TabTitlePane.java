package pl.katowice.poldrag.swiss.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;
import static pl.katowice.poldrag.swiss.languages.Languages._;

public class TabTitlePane extends JPanel
implements ActionListener, MouseListener {
    
    private final JTabbedPane pane;
    private static final ImageIcon iconClose = OSwiss.getMainForm().getImageIcon("12_close.png");

    private JButton createCloseButton()
    {
        JButton button = new JButton(iconClose);
        int size = 17;
        button.setPreferredSize(new Dimension(size, size));
        button.setToolTipText(_("MAINFORM_CLOSE_TOOLTIP"));
        
        button.setOpaque(false);
        button.setFocusPainted(false);
        button.setUI(new BasicButtonUI());
        button.setFocusable(false);
        button.setBorder(BorderFactory.createEtchedBorder());
        button.setBorderPainted(false);
        button.addMouseListener(this);
        button.setRolloverEnabled(true);
        button.addActionListener(this);
        
        return button;
    }
    
    
    public TabTitlePane(final JTabbedPane pane) {
        //unset default FlowLayout' gaps
        super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        if (pane == null) {
            throw new NullPointerException("TabbedPane is null");
        }
        this.pane = pane;
        setOpaque(false);
        
        //make JLabel read titles from JTabbedPane
        JLabel label = new JLabel() {
            @Override
            public String getText() {
                int i = pane.indexOfTabComponent(TabTitlePane.this);
                if (i != -1) {
                    return pane.getTitleAt(i);
                }
                return null;
            }
        };
        
        add(label);
        //add more space between the label and the button
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        //tab button
        JButton button = createCloseButton();
        add(button);
        //add more space to the top of the component
        setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        int i = pane.indexOfTabComponent(TabTitlePane.this);
        if (i != -1) {
            boolean confirmed = ((TournamentPane)pane.getComponentAt(i)).closeTournament();
            if (confirmed)
                pane.remove(i);
        }
    }


    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        Component component = e.getComponent();
        if (component instanceof AbstractButton) {
            AbstractButton button = (AbstractButton) component;
            button.setBorderPainted(true);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
       Component component = e.getComponent();
       if (component instanceof AbstractButton) {
            AbstractButton button = (AbstractButton) component;
            button.setBorderPainted(false);
        }
    }


}


