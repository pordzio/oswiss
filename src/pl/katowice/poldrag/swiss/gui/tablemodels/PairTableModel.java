package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import pl.katowice.poldrag.swiss.core.Pair;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.core.Result;
import pl.katowice.poldrag.swiss.gui.OSwiss;
import pl.katowice.poldrag.swiss.gui.PairListPane;
import pl.katowice.poldrag.swiss.gui.TournamentPane;
import static pl.katowice.poldrag.swiss.languages.Languages._;


public class PairTableModel extends AbstractTableModel {

    private List<Pair> pairList;
    private static String[] columnNames = null;
    private boolean editable;
    private TournamentPane tpane;
    private PairListPane plpane;


    public PairTableModel(TournamentPane tpane,
            PairListPane plpane, List<Pair> pairList)
    {
        this.tpane = tpane;
        this.plpane = plpane;
        this.pairList = pairList;
        this.editable = true;
        
        if (columnNames == null) {
            columnNames = new String[] {
                _("TABLE_NO"), _("TABLE_PLAYER_1"), _("TABLE_PLAYER_2"),
                _("TABLE_RESULT")
            };
        }
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return pairList.size();
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public Object getValueAt(int row, int col) {

        Pair pair = pairList.get(row);

        switch (col) {
            case 0: return row+1;
            case 1: return pair.getPlayer1();
            case 2: return pair.getPlayer2();
            case 3: return pair.getResult();
        }
        
        return null;
    }
    
    @Override
    public Class getColumnClass(int c) {
        switch (c) {
            case 0: return Integer.class;
            case 1: return Player.class;
            case 2: return Player.class;
            case 3: return Result.class;
        }

        return String.class;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        if (!editable)
            return false;
        
        if (col == 3)
            return true;
                   
        if (col == 1 && pairList.get(row).getPlayer1().getPublicId() >= 0)
            return true;

        if (col == 2 && pairList.get(row).getPlayer2().getPublicId() >= 0)
            return true;

        return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        
        if (!editable)
            return;
                
        if (col == 3) {
            pairList.get(row).setPoints((Result)value);
            tpane.refreshStatus(plpane);
            return;
        }
            
        Player editedPlayer;
        if (col == 1)
            editedPlayer = pairList.get(row).getPlayer1();
        else
            editedPlayer = pairList.get(row).getPlayer2();
            
        String newName = ((Player)value).getName();
        List<Player> playerList = tpane.getTournament().getPlayerList();
        for (Player pl : playerList) {
            if (pl.getName().equals(newName) && pl != editedPlayer) {
                JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                    _("ERR_PLAYER_DUP_MSG"),
                    _("ERR_PLAYER_DUP_TITLE"),
                    JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        
        editedPlayer.setName(newName);
        tpane.refreshAllTables();
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
}
