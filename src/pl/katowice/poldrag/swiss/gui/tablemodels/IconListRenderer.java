/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import pl.katowice.poldrag.swiss.gui.OSwiss;
import pl.katowice.poldrag.swiss.gui.TableCardPane;
import static pl.katowice.poldrag.swiss.languages.Languages._;

/**
 *
 * @author kkopec
 */
    
public class IconListRenderer 
extends DefaultListCellRenderer {

    private static ImageIcon iconOK = OSwiss.getMainForm().getImageIcon("16_validate_ok.png");
    private static ImageIcon iconProgress = OSwiss.getMainForm().getImageIcon("16_validate_progress.png");
    private static ImageIcon iconFail = OSwiss.getMainForm().getImageIcon("16_validate_fail.png");
    
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, 
            boolean isSelected, boolean cellHasFocus) {
		
        JLabel label = (JLabel) super.getListCellRendererComponent(list, 
            value, index, isSelected, cellHasFocus);

        switch(((TableCardPane)value).getStatus()) {
            case TableCardPane.STATUS_OK:
                label.setIcon(iconOK);
                label.setToolTipText(_("TOOLTIP_STATUS_OK"));
                break;
            case TableCardPane.STATUS_PROGRESS:
                label.setIcon(iconProgress);
                label.setToolTipText(_("TOOLTIP_STATUS_PROGRESS"));
                break;
            default:
            case TableCardPane.STATUS_FAIL:
                label.setIcon(iconFail);
                label.setToolTipText(_("TOOLTIP_STATUS_FAIL"));
                break;
        }
        
        return label;
    }
}
