package pl.katowice.poldrag.swiss.gui.tablemodels;

import javax.swing.table.AbstractTableModel;
import pl.katowice.poldrag.swiss.core.KValues;
import static pl.katowice.poldrag.swiss.languages.Languages._;

public class RankingTableModel extends AbstractTableModel {

    private KValues kValues = null;
    private static String[] columnNames = null;

    public RankingTableModel(KValues kValues) {
        this.kValues = kValues;

        if (columnNames == null) {
            columnNames = new String[]{
                _("TABLE_NUM_OF_PLAYERS"), _("TABLE_K_VALUE")
            };
        }
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public int getRowCount() {
        return kValues.size();
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        KValues.Entry entry;

        entry = kValues.getEntry(row);
        if (entry != null) {
            return col == 0 ? entry.getPlayers() : entry.getkValue();
        }

        return null;
    }

    /*
     * JTable uses this method to determine the default renderer/
     * editor for each cell.  If we didn't implement this method,
     * then the last column would contain text ("true"/"false"),
     * rather than a check box.
     */
    @Override
    public Class getColumnClass(int c) {
        return PositiveInteger.class;
    }

    /*
     * Don't need to implement this method unless your table's
     * editable.
     */
    @Override
    public boolean isCellEditable(int row, int col) {
        return true;
    }

    /*
     * Don't need to implement this method unless your table's
     * data can change.
     */
    @Override
    public void setValueAt(Object value, int row, int col) {

        Integer newValue;

        try {
            newValue = ((PositiveInteger) value).getValue();

            // don't allow to set value equal to 0
            if (newValue == 0) {
                return;
            }

            // can't change the first value of the table, it must be 1
            if (row == 0 && col == 0) {
                newValue = 1;
            }
            
            KValues.Entry entry = kValues.getEntry(row);
            if (entry == null) {
                throw new Exception();
            }

            if (col == 0) {
                // reinsert entry in the kvalue treemap
                kValues.remove(entry.getPlayers());
                kValues.put(newValue, entry.getkValue());
            } else {
                entry.setkValue(newValue);
            }
            
            fireTableDataChanged();

        } catch (Exception e) {
        }
    }
}
