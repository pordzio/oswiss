/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.util.Comparator;
import pl.katowice.poldrag.swiss.core.Player;

/**
 *
 * @author kkopec
 */
public class PlayerComparator implements Comparator<Player> {

    @Override
    public int compare(Player t, Player t1) {
        if (t == null || t1 == null)
            return 0;
        
        String name1 = t.getName().toLowerCase();
        String name2 = t1.getName().toLowerCase();
        
        return name1.compareTo(name2);
    }
    
}
