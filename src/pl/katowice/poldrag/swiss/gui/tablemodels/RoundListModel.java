/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui.tablemodels;

import javax.swing.DefaultListModel;

/**
 *
 * @author kkopec
 */
public class RoundListModel extends DefaultListModel {

    public void fireContentsChanged() {
        super.fireContentsChanged(this, 0, this.size()-1);
    }
    
    public void fireContentsChanged(int index) {
        super.fireContentsChanged(this, index, index);
    }
    
}
