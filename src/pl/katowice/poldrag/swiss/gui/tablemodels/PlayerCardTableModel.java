package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import pl.katowice.poldrag.swiss.core.Pair;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.core.Result;
import static pl.katowice.poldrag.swiss.languages.Languages._;


public class PlayerCardTableModel extends AbstractTableModel {

    private static String[] columnNames = null;
    private Player player;
    private List<Pair> pairList;


    public PlayerCardTableModel(Player player, List<Pair> pairList)
    {
        this.player = player;
        this.pairList = pairList;
        
        if (columnNames == null) {
            columnNames = new String[] {
                _("TABLE_NO"), _("TABLE_PLAYER_1"), _("TABLE_PLAYER_2"),
                _("TABLE_RESULT"), _("TABLE_STAKE")
            };
        }
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return pairList.size();
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public Object getValueAt(int row, int col) {

        Pair pair = pairList.get(row);

        switch (col) {
            case 0: return row+1;
            case 1: return pair.getPlayer1();
            case 2: return pair.getPlayer2();
            case 3: return pair.getResult();
            case 4: 
                if (player == pair.getPlayer1())
                    return pair.getStake();
                else
                    return -pair.getStake();
        }
        
        return null;
    }
    
    @Override
    public Class getColumnClass(int c) {
        switch (c) {
            case 0: return Integer.class;
            case 1: return Player.class;
            case 2: return Player.class;
            case 3: return Result.class;
            case 4: return Integer.class;
        }

        return String.class;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
}
