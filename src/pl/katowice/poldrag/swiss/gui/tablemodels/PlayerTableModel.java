package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.core.Points;
import pl.katowice.poldrag.swiss.gui.OSwiss;
import pl.katowice.poldrag.swiss.gui.TournamentPane;
import static pl.katowice.poldrag.swiss.languages.Languages._;

public class PlayerTableModel extends AbstractTableModel {

    private List<Player> playerList;
    
    private static String[] columnNames = null;
    private boolean editable;
    private TournamentPane tpane;


    public PlayerTableModel(TournamentPane tpane,
            List<Player> playerList)
    {
        this.tpane = tpane;
        this.playerList = playerList;
        this.editable = true;
        
        if (columnNames == null) {
            columnNames = new String[] {
                _("TABLE_ID"), _("TABLE_PLAYER"), _("TABLE_RANKING"),
                _("TABLE_POINTS")
            };
        }
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return playerList.size();
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        
        Player pl = playerList.get(row);
        
        switch (col) {
            case 0: return row+1;
            case 1: return pl;
            case 2: return pl.getRanking();
            case 3: return pl.getPoints();
        }
        
        return null;
    }
    
    @Override
    public Class getColumnClass(int c) {
        switch (c) {
            case 0: return Integer.class;
            case 1: return Player.class;
            case 2: return PositiveInteger.class;
            case 3: return Points.class;
        }

        return String.class;
    }
    
    @Override
    public boolean isCellEditable(int row, int col) {
        if (editable && col == 1)
            return true;

        return false;
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {

        if (!editable)
            return;
        
        if (col == 1) {
            String newName = ((Player)value).getName();
            
            for (Player pl : playerList) {
                if (pl.getName().equals(newName) && pl != playerList.get(row)) {
                    JOptionPane.showMessageDialog(OSwiss.getMainForm(),
                        _("ERR_PLAYER_DUP_MSG"),
                        _("ERR_PLAYER_DUP_TITLE"),
                        JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            playerList.get(row).setName(newName);
            tpane.refreshAllTables();
        }
        
        // ranking is not editable
        //else if (col == 2)
        //    playerList.get(row).setRanking(((PositiveInteger)value).getValue());
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }
    
}
