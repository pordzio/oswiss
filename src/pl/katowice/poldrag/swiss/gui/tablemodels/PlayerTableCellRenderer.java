/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.awt.Color;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import pl.katowice.poldrag.swiss.core.Player;
import static pl.katowice.poldrag.swiss.languages.Languages._;

/**
 *
 * @author kkopec
 */
public class PlayerTableCellRenderer 
extends BorderedTableCellRenderer {
    
    protected static ImageIcon getImageIcon(String fileName) {
        return new ImageIcon(
                PlayerTableCellRenderer.class.getResource("/pl/katowice/poldrag/swiss/icons/" + fileName)
        );
    }
    
    private static final Color lightGray = new Color(127, 184, 226);
    private static final ImageIcon iconRemote = getImageIcon("16_player_remote.png");
    private static final ImageIcon iconLocal = getImageIcon("16_player_local.png");
    
    @Override
    public Component getTableCellRendererComponent(
          JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        
        JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (table.isPaintingForPrint()) {
            label.setForeground(Color.BLACK);
            label.setIcon(null);
            return label;
        }
        
        label.setHorizontalTextPosition(SwingConstants.LEFT);
        Player pl = (Player)value;
        
        if (pl.getFile() != null) {
            if (pl.getPublicId() > 0)
                label.setIcon(iconRemote);
            else
                label.setIcon(iconLocal);
        } else
           label.setIcon(null);
        
        if (pl.isLeaved()) {
            label.setForeground(lightGray);
            label.setText(pl.getName() + " (" + _("TABLE_LEAVED")+")");
        } else {
            label.setForeground(Color.BLACK);
        }

        return label;
    }
    
}
