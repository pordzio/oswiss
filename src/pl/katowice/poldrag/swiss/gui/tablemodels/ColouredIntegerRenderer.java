/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author kkopec
 */
public class ColouredIntegerRenderer extends DefaultTableCellRenderer {
 
    private static final Color colorGreen = new Color(0, 153, 51);
    private static final Color colorRed = new Color(153, 51, 0);
 
    @Override
    public Component getTableCellRendererComponent(
          JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        
        JLabel label = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setFont(label.getFont().deriveFont(Font.BOLD));
        
        if (table.isPaintingForPrint()) {
            label.setForeground(Color.BLACK);
            return label;
        }
        
        try {
            Integer i = (Integer)value;
            
            if (i > 0)
                label.setForeground(colorGreen);
            else if (i < 0)
                label.setForeground(colorRed);
            else
                label.setForeground(Color.BLACK);
            
        } catch (ClassCastException cce) {
            label.setForeground(Color.BLACK);
        }
        
        return label;
    }
    
}
