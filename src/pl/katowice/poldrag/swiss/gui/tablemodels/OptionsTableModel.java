package pl.katowice.poldrag.swiss.gui.tablemodels;

import javax.swing.table.AbstractTableModel;
import pl.katowice.poldrag.swiss.core.Result;
import static pl.katowice.poldrag.swiss.languages.Languages._;

public class OptionsTableModel extends AbstractTableModel {
    
    private Result resultList[] = null;
    
    private static String resultDesc[] = null;
    private static String[] columnNames = null;

    public OptionsTableModel(Result resultList[])
    {
        this.resultList = resultList;

        if (resultDesc == null) {
            resultDesc = new String[] {
                _("TABLE_WIN"), _("TABLE_LOSS"), _("TABLE_DRAW"),
                _("TABLE_PAUSE"), _("TABLE_MISS")
            };
        }
        
        if (columnNames == null) {
            columnNames = new String[] {
                _("TABLE_MATCH_RESULT"), _("TABLE_POINTS_1"), _("TABLE_POINTS_2")
            };
        }
    }
    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return resultList.length;
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        switch (col) {
            case 0: return resultDesc[row];
            case 1: return resultList[row].getPoints1();
            case 2: return resultList[row].getPoints2();
        }
        
        return null;
    }
    
    /*
    * JTable uses this method to determine the default renderer/
    * editor for each cell.  If we didn't implement this method,
    * then the last column would contain text ("true"/"false"),
    * rather than a check box.
    */
    @Override
    public Class getColumnClass(int c) {
        switch (c) {
            case 0: return String.class;
            case 1: return PositiveInteger.class;
            case 2: return PositiveInteger.class;
        }

        return String.class;
    }
    
    /*
    * Don't need to implement this method unless your table's
    * editable.
    */
    @Override
    public boolean isCellEditable(int row, int col) {
        if (col == 1 || col == 2)
            return true;

        return false;
    }
    
    /*
    * Don't need to implement this method unless your table's
    * data can change.
    */
    @Override
    public void setValueAt(Object value, int row, int col) {
        
        int points;
        try {
            points = ((PositiveInteger)value).getValue();
        } catch (Exception e) {
            return;
        }
    
        if (col == 1)
            resultList[row].setPoints1(points);
        
        else if (col == 2)
            resultList[row].setPoints2(points);
    }

}
