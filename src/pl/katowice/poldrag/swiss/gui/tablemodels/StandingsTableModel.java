package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.core.Points;
import static pl.katowice.poldrag.swiss.languages.Languages._;

public class StandingsTableModel extends AbstractTableModel {

    private List<Player> standings;
    
    private String[] columnNames = null;
    
    public StandingsTableModel(List<Player> standings)
    {
        this.standings = standings;

        if (columnNames == null) {
            columnNames = new String[] {
                _("TABLE_NO"), _("TABLE_PLAYER"), _("TABLE_OLD_RANKING"),
                _("TABLE_NEW_RANKING"), _("TABLE_POINTS")
            };
        }
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    
    @Override
    public int getRowCount() {
        return standings.size();
    }
    
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    
    @Override
    public Object getValueAt(int row, int col) {
        Player pl = standings.get(row);
       
        switch (col) {
            case 0: return row+1;
            case 1: return pl;
            case 2: return pl.getRanking();
            case 3: return pl.getNewRanking();
            case 4: return pl.getPoints();
        }
        
        return null;
    }
    
    /*
    * JTable uses this method to determine the default renderer/
    * editor for each cell.  If we didn't implement this method,
    * then the last column would contain text ("true"/"false"),
    * rather than a check box.
    */
    @Override
    public Class getColumnClass(int c) {
        switch (c) {
            case 0: return Integer.class;
            case 1: return Player.class;
            case 2: return Integer.class;
            case 3: return Integer.class;
            case 4: return Points.class;
        }

        return String.class;
    }
    
    /*
    * Don't need to implement this method unless your table's
    * editable.
    */
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
}
