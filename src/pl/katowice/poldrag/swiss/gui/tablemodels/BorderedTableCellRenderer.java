/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui.tablemodels;

import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author kkopec
 */
public class BorderedTableCellRenderer extends CompoundBorder
    implements TableCellRenderer {
 
    /** Serial version UID */
    private static final long serialVersionUID = 1L;
    private final TableCellRenderer renderer;
 
    public BorderedTableCellRenderer() {
        this.renderer = new DefaultTableCellRenderer();
        this.insideBorder = BorderFactory.createEmptyBorder(0, 5, 0, 0);
    }
 
    @Override
    public Component getTableCellRendererComponent(
          JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        
        final Component comp =
            renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if (comp instanceof JComponent) {
            final JComponent jcomp = (JComponent) comp;
            final Border orgBorder = jcomp.getBorder();
            this.outsideBorder = orgBorder;
            jcomp.setBorder(this);
        }
        
        return comp;
    }
    
}
