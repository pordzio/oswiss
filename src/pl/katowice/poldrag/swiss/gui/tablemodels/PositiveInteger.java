/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.gui.tablemodels;

/**
 *
 * @author kkopec
 */
public class PositiveInteger 
implements Comparable {
    
    private Integer value;
    
    public PositiveInteger(String s) {
        value = new Integer(s);
        if (value < 0)
            throw new IllegalArgumentException();
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public int compareTo(Object t) {
        return value.compareTo(((PositiveInteger)t).getValue());
    }
}
