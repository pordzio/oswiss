package pl.katowice.poldrag.swiss.core;

import java.util.ArrayList;
import java.util.List;

public class Round {

    private List<Pair> pairs;
    private int number;
    private int top = 0;

    public Round()
    {
        pairs = new ArrayList<Pair>();
    }

    public List<Pair> getPairs() {
        return pairs;
    }

    public Pair getPair(int ind)
    {
        return pairs.get(ind);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean allResultsFilled()
    {
        for (Pair pair : pairs)
            if (!pair.isSet())
                return false;

        return true;
    }

    public void addPair(Player player1, Player player2)
    {
        pairs.add(new Pair(player1, player2) );
    }

    public void addPair(Pair p)
    {
        pairs.add(p);
    }

    public Pair removeLastPair()
    {
        if (pairs.size() <= 0)
            return null;

        Pair p = pairs.get(pairs.size() - 1);
        pairs.remove(pairs.size() - 1);
        return p;
    }

    public Pair getPairWithPlayer(Player player) {

        for (Pair p : pairs) {
            if (p.getPlayer1() == player || p.getPlayer2() == player)
                return p;
        }

        return null;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public boolean isTop() {
        return this.top != 0;
    }

}
