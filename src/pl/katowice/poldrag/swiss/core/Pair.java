package pl.katowice.poldrag.swiss.core;


public class Pair {

    private Player player1;
    private Player player2;
    private Result result;
    private int stake;

    public Pair(Player player1, Player player2)
    {
        this.player1 = player1;
        this.player2 = player2;
        result = null;

        // automatic set results form players Miss and Pause
        if (player1 == Player.playerPause)
            result = new Result(Result.resultRevPause);

        if (player2 == Player.playerPause)
            result = new Result(Result.resultPause);

        if (player1 == Player.playerMiss)
            result = new Result(Result.resultRevMiss);

        if (player2 == Player.playerMiss)
            result = new Result(Result.resultMiss);
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public int getPoints1() {
        return result.getPoints1();
    }

    public int getPoints2() {
        return result.getPoints2();
    }

    public void setPoints(int s1, int s2) {
        result = new Result(s1, s2);
    }

    public void setPoints(Result result)
    {
        this.result = result;
    }

    public Result getResult() {
        return result;
    }

    public int getStake() {
        return stake;
    }

    public void setStake(int stake) {
        this.stake = stake;
    }

    public void resetPoints() {
        this.result = null;
    }

    public boolean isSet()
    {
        return (result != null);
    }

    public Player getWinner() {
        if (player1 == Player.playerMiss)
            return player2;

        if (player2 == Player.playerMiss)
            return player1;

        if (result == null)
            return null;

        if (result.getPoints1() > result.getPoints2())
            return player1;

        if (result.getPoints1() < result.getPoints2())
            return player2;

        return null;
    }

    public Player getLosser() {
        if (player1 == Player.playerMiss)
            return player1;

        if (player2 == Player.playerMiss)
            return player2;

        if (result == null)
            return null;

        if (result.getPoints1() < result.getPoints2())
            return player1;

        if (result.getPoints1() > result.getPoints2())
            return player2;

        return null;
    }

    @Override
    public String toString() {
        return "Pair{" + "player1=" + player1.getName() +
               ", player2=" + player2.getName() +
               ", result=(" + result.getPoints1() +":"+ result.getPoints2() + ")}";
    }



}

