package pl.katowice.poldrag.swiss.core;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.io.File;
import static pl.katowice.poldrag.swiss.languages.Languages._;

/**
 *
 * @author Buizel
 */
public class Player
implements Comparable
{
    public static String PLAYER_NAME_PAUSE;
    public static String PLAYER_NAME_MISS;

    public static final int PLAYER_NEW_PLAYER = 0;
    public static final int PLAYER_ID_PAUSE = -1;
    public static final int PLAYER_ID_MISS = -2;

    public static Player playerPause;
    public static Player playerMiss;

    public static void initStaticPlayers() {
        PLAYER_NAME_PAUSE = _("PLAYER_NAME_PAUSE");
        PLAYER_NAME_MISS = _("PLAYER_NAME_MISS");

        playerPause = new Player(PLAYER_NAME_PAUSE, PLAYER_ID_PAUSE);
        playerMiss = new Player(PLAYER_NAME_MISS, PLAYER_ID_MISS);
    }

    @XStreamOmitField
    private int reserved1;
    @XStreamOmitField
    private int reserved2;

    private String name;
    private int ranking;
    private int newRanking;
    private Points points;
    private boolean leaved;
    private int publicId;

    @XStreamOmitField
    private File file;
    private String localPath;


    public static boolean validateName(String name)
    {
        if (name.equals("") ||
            name.equals(Player.PLAYER_NAME_PAUSE) ||
            name.equals(Player.PLAYER_NAME_MISS) ||
            !name.matches("[\\p{L}._-]+|[\\p{L}._-]+[\\p{L}\\s._-]*[\\p{L}._-]+"))
            return false;

        return true;
    }


    public Player(String name)
    {
        this(name, 0);

        if (!validateName(name.trim()))
            throw new IllegalArgumentException();
    }

    public Player(Player pl)
    {
        reserved1 = pl.reserved1;
        reserved2 = pl.reserved2;
        ranking = pl.ranking;
        newRanking = pl.newRanking;
        points = pl.points;
        file = pl.file;
        localPath = pl.localPath;
        name = pl.name;
        publicId = pl.publicId;
        leaved = pl.leaved;
    }

    public Player(String name, int publicId)
    {
        reserved1 = 0;
        reserved2 = 0;
        ranking = newRanking = AbstractTournament.getInitialRanking();
        points = new Points();
        file = null;
        localPath = null;
        resetPoints();
        this.name = name.trim();
        this.publicId = publicId;
        leaved = false;
    }

    public final void resetPoints()
    {
        points.reset();
    }

    public Points getPoints()
    {
        return points;
    }

    public int getPoints(int no)
    {
        return points.get(no);
    }

    public void setPoints(int no, int val)
    {
        points.setPoints(no, val);
    }

    public void addPoints(int no, int val)
    {
        points.addPoints(no, val);
    }

    public void copyPoints(Player player) {
        for (int i=0; i<Points.TYPES; i++)
            this.points.setPoints(0, player.points.get(0));
    }

    public int getReserved1() {
        return reserved1;
    }

    public void setReserved1(int id) {
        this.reserved1 = id;
    }

    public int getReserved2() {
        return reserved2;
    }

    public void setReserved2(int reserved2) {
        this.reserved2 = reserved2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name)
    {
        this.name = name.trim();
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking < 100 ? 100 : ranking;
    }

    public int getNewRanking() {
        return newRanking;
    }

    public void setNewRanking(int newRanking) {
        this.newRanking = newRanking;
    }

    public int getPublicId() {
        return publicId;
    }

    public void setPublicId(int publicId) {
        this.publicId = publicId;
    }

    public File getFile() {

        if (file != null)
            return file;

        if (localPath == null)
            return null;

        File f = new File(localPath);
        if (f.exists()) {
            file = f;
            return file;
        }

        return null;
    }

    public void setFile(File file) {
        localPath = file.getPath();
        this.file = file;
    }

    public boolean isLeaved() {
        return leaved;
    }

    public void setLeaved(boolean leaved) {
        this.leaved = leaved;
    }

    @Override
    public String toString()
    {
        return(name);
    }

    @Override
    public int compareTo(Object t)
    {
        Player p;

        try {
            p = (Player)t;
        } catch (ClassCastException cce) {
            return 0;
        }

        for (int i=0; i<Points.TYPES; i++)
            if (getPoints(i) != p.getPoints(i))
                return p.getPoints(i) - getPoints(i);

        /* if players has the same tie-breaks, then they are equal
         * we shoudn't use ranking to evaluation as: return p.getRanking() - getRanking()
         */
        return 0;
    }

}
