/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.core;

/**
 *
 * @author kkopec
 */
public class Points implements Comparable
{

    /* there is 4 types of points in memory from A to D */
    public static int TYPES = 4;

    int pointsA;
    int pointsB;
    int pointsC;
    int pointsD;

    public Points() {
        pointsA = pointsB = pointsC = pointsD = 0;
    }

    public void reset() {
        pointsA = pointsB = pointsC = pointsD = 0;
    }

    public void setPoints(int ind, int val) {
        switch(ind) {
            case 0: pointsA = val; break;
            case 1: pointsB = val; break;
            case 2: pointsC = val; break;
            case 3: pointsD = val; break;
        }
    }

    public void addPoints(int ind, int val) {
        switch(ind) {
            case 0: pointsA += val; break;
            case 1: pointsB += val; break;
            case 2: pointsC += val; break;
            case 3: pointsD += val; break;
        }
    }

    public int get(int ind) {
        switch(ind) {
            case 0: return pointsA;
            case 1: return pointsB;
            case 2: return pointsC;
            case 3: return pointsD;
        }

        return 0;
    }
    
    @Override
    public String toString() {

        return  Integer.toString(pointsA) + " - " +
                Integer.toString(pointsB) + " - " +
                Integer.toString(pointsC);
    }

    @Override
    public int compareTo(Object t) {
        Points p;

        try {
            p = (Points)t;
        } catch (ClassCastException cce) {
            return 0;
        }

        for (int i=0; i<Points.TYPES; i++)
            if (get(i) != p.get(i))
                return p.get(i) - get(i);

        return 0;
    }

}
