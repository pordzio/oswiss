/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.core;

/**
 *
 * @author kkopec
 */
public class Result {
    
    public static Result resultLoss = null;
    public static Result resultDraw = null;
    public static Result resultWin = null;

    public static Result resultPause = null;
    public static Result resultRevPause = null;
    public static Result resultMiss = null;
    public static Result resultRevMiss = null;
    
    private int points1;
    private int points2;
    
    public Result(int points1, int points2) {
        this.points1 = points1;
        this.points2 = points2;
    }
    
    public Result(Result result) {
        if (result == null)
            points1 = points2 = 0;
        else {
            this.points1 = result.points1;
            this.points2 = result.points2;
        }
    }
    
    public Result(String s) {
        Result res = parseRes(s);
        if (res == null)
            throw new IllegalArgumentException();
        
        this.points1 = res.points1;
        this.points2 = res.points2;
    }    

    public int getPoints1() {
        return points1;
    }

    public int getPoints2() {
        return points2;
    }

    public void setPoints1(int points1) {
        this.points1 = points1;
    }

    public void setPoints2(int points2) {
        this.points2 = points2;
    }
    
    public void revert() {
        int tmp = points1;
        points1 = points2;
        points2 = tmp;
    }

    @Override
    public String toString() {
        return "(" + points1 + " - " + points2 + ")";
    }
    
    public static Result parseRes(String s) {

        int p1, p2;
        Result res;
        
        if (s == null)
            return null;
        
        try {
            s = s.replaceAll("\\s", "");
            if (s.charAt(0) != '(' || s.charAt(s.length()-1) != ')')
                return null;

            // delete brackets
            s = s.substring(1, s.length()-1);

            int i = s.indexOf('-');
            p1 = Integer.parseInt(s.substring(0,i));
            p2 = Integer.parseInt(s.substring(i+1));

            if (p1 < 0 || p2 < 0)
                return null;
            
            res = new Result(p1, p2);
        } catch (Exception e) {
            return null;
        }
        
        return res;
    }
}
