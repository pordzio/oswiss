/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.core;

import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author kkopec
 */
public class KValues extends TreeMap<Integer, Integer> {

    public static class Entry {
        private Map.Entry<Integer, Integer> entry;

        public Entry(Map.Entry<Integer, Integer> entry) {
            this.entry = entry;
        }

        public Entry(Integer players, Integer kValue) {
            this.entry = new TreeMap.SimpleEntry<Integer, Integer>(players, kValue);
        }

        public Integer getPlayers() {
            return entry.getKey();
        }

        public Integer getkValue() {
            return entry.getValue();
        }

        public Integer setkValue(Integer kValue) {
            return entry.setValue(kValue);
        }
    }

    public Integer put(KValues.Entry entry) {
        return super.put(entry.getPlayers(), entry.getkValue());
    }
    
    public Integer removePosition(int position) {
        // don't allow to delete if only one entry
        if (size() <= 1) {
            return null;
        }

        KValues.Entry entry = getEntry(position);
        
        if (entry != null) {
            Integer removedValue = remove(entry.getPlayers());
            
            // update the second row to value "1"
            if (position == 0) {
                entry = getEntry(0);
                remove(entry.getPlayers());
                put(1, entry.getkValue());
            }
            
            return removedValue;
        }

        return null;
    }

    public KValues.Entry getEntry(int position) {
        int currentPosition = 0;

        if (position >= size()) {
            return null;
        }

        for (Map.Entry<Integer, Integer> entry : entrySet()) {
            if (currentPosition++ < position) {
                continue;
            }
            return new KValues.Entry(entry);
        }

        return null;
    }
    
    public int getKValue(int players) {
        
        int kVal = 1;  // default k-value
        
        for (Map.Entry<Integer, Integer> entry : entrySet()) {
            if (entry.getKey() > players) {
                break;
            }
            kVal = entry.getValue();
        }

        return kVal;
    }
    
    public String serialize()
    {
        StringBuilder kValues = new StringBuilder();
        for (Map.Entry<Integer, Integer> entry : entrySet()) {
            kValues.append('(');
            kValues.append(entry.getKey());
            kValues.append(',');
            kValues.append(entry.getValue());
            kValues.append(')');
        }
        
        return kValues.toString();
    }
    
    public static KValues unserialize(String kValuesString)
    {
        KValues kValues = new KValues();

        Pattern pattern = Pattern.compile("\\((\\d+),(\\d+)\\)");
        Matcher matcher = pattern.matcher(kValuesString);

        try {
            while (matcher.find()) {
                kValues.put(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
            }
        } catch (NumberFormatException nfe) {
            kValues.clear();
        }
        return kValues;
    }
    
    @Override
    public KValues clone() {
        return unserialize(this.serialize());
    }
    
    public void copy(KValues kValues) {
        kValues.clear();
        for (Map.Entry<Integer, Integer> entry : entrySet()) {
            kValues.put(entry.getKey(), entry.getValue());
        }
    }

}
