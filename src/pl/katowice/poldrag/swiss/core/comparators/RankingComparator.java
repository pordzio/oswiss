package pl.katowice.poldrag.swiss.core.comparators;

import java.util.Comparator;
import pl.katowice.poldrag.swiss.core.Player;

/**
 *
 * @author kkopec
 */
public class RankingComparator implements Comparator<Player> {


    @Override
    public int compare(Player t, Player t1) {
        if (t == null || t1 == null)
            return 0;

        return t1.getRanking() - t.getRanking();
    }

}

