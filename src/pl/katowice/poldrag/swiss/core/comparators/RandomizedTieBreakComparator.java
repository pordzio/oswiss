package pl.katowice.poldrag.swiss.core.comparators;

import java.util.Comparator;
import pl.katowice.poldrag.swiss.core.Player;

/**
 *
 * @author kkopec
 */
public class RandomizedTieBreakComparator implements Comparator<Player> {


    @Override
    public int compare(Player t, Player t1) {
        if (t == null || t1 == null)
            return 0;
        
        /* we are comparing only the first tie-breaker (progress)
           the players will be in random order inside groups with
           the same progress. */
        return t1.getPoints(0) - t.getPoints(0);
    }

}
