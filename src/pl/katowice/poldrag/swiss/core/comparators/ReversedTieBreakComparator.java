package pl.katowice.poldrag.swiss.core.comparators;

import java.util.Comparator;
import pl.katowice.poldrag.swiss.core.Player;
import pl.katowice.poldrag.swiss.core.Points;

/**
 *
 * @author kkopec
 */
public class ReversedTieBreakComparator implements Comparator<Player> {

    @Override
    public int compare(Player t, Player t1) {
        if (t == null || t1 == null)
            return 0;

        // the same comparation as in Player.compare(), but reversed
        for (int i=0; i<Points.TYPES; i++)
            if (t.getPoints(i) != t1.getPoints(i))
                return t.getPoints(i) - t1.getPoints(i);

        /* if players has the same tie-breaks, then they are equal
         * we shoudn't use ranking to evaluation as: return t.getRanking() - t1.getRanking()
         */
        return 0;
    }

}