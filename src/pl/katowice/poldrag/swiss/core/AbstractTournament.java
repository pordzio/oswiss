/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.core;

import java.util.ArrayList;
import java.util.List;
import pl.katowice.poldrag.swiss.exceptions.*;

/**
 *
 * @author kkopec
 */
public abstract class AbstractTournament
implements Tournament {

    public static final int ALGORITHM_ELO_ROUND = 0;
    public static final int ALGORITHM_ELO_TOURNAMENT = 1;

    /* gracze zapisani na turniej */
    protected List<Player> players;

    protected List<Player> standings;

    /* lista rozgrywanych w turnieju rund */
    protected List<Round> rounds;

    protected boolean isFinished;

    public static int confRankingAlgorithm;
    public static KValues confRankingKValues;
    public static int confRankingInitial;

    static int getInitialRanking() {
        return confRankingInitial;
    }


    @Override
    public boolean addPlayer(Player player)
    throws SwissPlayerNameCollision, SwissIllegalPlayerName, SwissTournamentFinished
    {
        boolean alreadyAdded = false;
        boolean missAdded = false;

        if (isFinished())
            throw new SwissTournamentFinished();

        if (player.getName().equals(Player.PLAYER_NAME_PAUSE)
           || player.getName().equals(Player.PLAYER_NAME_MISS))
            throw new SwissIllegalPlayerName();

        for (Player pl : players) {
            if (player.getName().equals(pl.getName())) {

                if (pl.isLeaved()) {
                    pl.setLeaved(false);
                    player = pl;
                    alreadyAdded = true;
                } else
                    throw new SwissPlayerNameCollision();
            }
        }

        if (!alreadyAdded)
            players.add(player);

        if (! rounds.isEmpty()) {
            for (Round r : rounds) {

                if (!r.isTop() && r.getPairWithPlayer(player) == null) {
                    r.addPair(player, Player.playerMiss);
                    missAdded = true;
                } else
                    missAdded = false;
            }
        }

        return missAdded;
    }

    public void delPlayer(Player pl)
    {
        boolean isPaired = false;
        Pair p;
        for (Round r : rounds) {
            if (( p = r.getPairWithPlayer(pl)) != null &&
               p.getPlayer1() != Player.playerMiss &&
               p.getPlayer2() != Player.playerMiss) {
                isPaired = true;
                break;
            }
        }

        if (isPaired) {
            pl.setLeaved(true);
        } else {
            for (Round r : rounds)
                if ((p = r.getPairWithPlayer(pl)) != null)
                    r.getPairs().remove(p);
            players.remove(pl);
        }
    }

    @Override
    public void delPlayer(int playerIndex)
    {
        try {
            delPlayer(players.get(playerIndex));
        } catch (IndexOutOfBoundsException e) {
        }
    }

    @Override
    public Round currentRound()
    {
        if (rounds.isEmpty())
            return null;

        return rounds.get(rounds.size() - 1);
    }

    @Override
    public int getRoundsCount() {
        return rounds.size();
    }

    @Override
    public Round getRound(int number)
    {
        if (number < 0 || number >= rounds.size())
            return null;

        return rounds.get(number);
    }


    @Override
    public List<Pair> getPlayerPairs(Player pl) {

        List<Pair> pairList = new ArrayList<Pair>();

        for (Round r : rounds) {
            Pair p = r.getPairWithPlayer(pl);
            if (p != null)
                pairList.add(p);
        }

        return pairList;
    }

    protected void calculateRanking() {

        for (Player pl : players)
            pl.setNewRanking(pl.getRanking());

        int kVal = confRankingKValues.getKValue(players.size());

        for (Round r : rounds)
            for (Pair p : r.getPairs()) {

                Player pl1 = p.getPlayer1();
                Player pl2 = p.getPlayer2();

                if (pl1 == Player.playerPause || pl1 == Player.playerMiss ||
                    pl2 == Player.playerPause || pl2 == Player.playerMiss)
                    continue;

                double rankDiff;
                if (confRankingAlgorithm == ALGORITHM_ELO_TOURNAMENT)
                    rankDiff = pl2.getRanking() - pl1.getRanking();
                else // if (confRankingAlgorithm == ALGORITHM_ELO_ROUND)
                    rankDiff = pl2.getNewRanking() - pl1.getNewRanking();

                if (rankDiff > 400.0)
                    rankDiff = 400.0;
                else if (rankDiff < -400.0)
                    rankDiff = -400.0;

                double winExp = 1.0 / (1 + Math.pow(10.0, rankDiff/400.0));

                int points1 = p.getPoints1();
                int points2 = p.getPoints2();

                double outcome;
                if (points1 == points2)
                    outcome = 0.5;
                else if (points1 == 0)
                    outcome = 0.0;
                else if (points2 == 0)
                    outcome = 1.0;
                else
                    outcome = (double)(points1) / (double)(points2 + points1);

                int stake = (int)Math.round((double)kVal * (outcome - winExp));
                p.setStake(stake);
                pl1.setNewRanking(pl1.getNewRanking() + stake);
                pl2.setNewRanking(pl2.getNewRanking() - stake);
            }
    }


    @Override
    public int findInitialTop() {
        if (rounds.isEmpty())
            return 0;

        int roundNum = rounds.size() - 1;
        int initTop = 0;
        while (roundNum >= 0 && rounds.get(roundNum).isTop()) {
            initTop = rounds.get(roundNum).getTop();
            roundNum--;
        }

        return initTop;
    }


    protected void validateRound(Round round)
    throws SwissResultsNotFilled, SwissIllegalPairs {
        if (! round.allResultsFilled())
            throw new SwissResultsNotFilled();

        Player.playerPause.setReserved2(0);

        for (int i=1; i<=players.size(); i++)
            players.get(i-1).setReserved2(0);

        for (Pair p : round.getPairs()) {

            // pairs like Pause-Miss or Miss-Miss are illegal
            if ((p.getPlayer1() == Player.playerPause || p.getPlayer1() == Player.playerMiss) &&
               (p.getPlayer2() == Player.playerPause || p.getPlayer2() == Player.playerMiss))
                throw new SwissIllegalPairs();

            // checking if player dont play several times in the round.
            if (p.getPlayer1() != Player.playerMiss) {

                if (p.getPlayer1().getReserved2() != 0)
                    throw new SwissIllegalPairs();

                p.getPlayer1().setReserved2(1);
            }

            if (p.getPlayer2() != Player.playerMiss) {

                if (p.getPlayer2().getReserved2() != 0)
                    throw new SwissIllegalPairs();

                p.getPlayer2().setReserved2(1);
            }
        }

        // checking if all players have been paired.
        if (!round.isTop()) {
            for (Player pl : players) {
                if (pl.getReserved2() == 0)
                    throw new SwissIllegalPairs();
            }
        } else {
            // should be the same amount of pairs as initTop
            int numOfPairs = round.getPairs().size();
            if ((numOfPairs & (numOfPairs-1)) != 0)
                throw new SwissIllegalPairs();
        }
    }

    @Override
    public List<Player> getPlayerList()
    {
        return players;
    }


    @Override
    public List<Player> getStandings() {
        return standings;
    }


    @Override
    public boolean isFinished() {
        return isFinished;
    }


    @Override
    public void resumeTournament() throws SwissTournamentFinished {
        if (!isFinished())
            throw new SwissTournamentFinished();

        standings.clear();
        isFinished = false;
    }
}
