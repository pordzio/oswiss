package pl.katowice.poldrag.swiss.core;

import pl.katowice.poldrag.swiss.core.comparators.RankingComparator;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import pl.katowice.poldrag.swiss.core.comparators.RandomizedTieBreakComparator;
import pl.katowice.poldrag.swiss.core.comparators.ReversedTieBreakComparator;
import pl.katowice.poldrag.swiss.exceptions.*;

public class Swiss
extends AbstractTournament {

    private static final int NOT_PLAYED = -1;

    public static final int ALGORITHM_BUCHHOLTZ_BERGER = 0;
    public static final int ALGORITHM_NWINS_BERGER = 1;
    public static final int ALGORITHM_NWINS_BUCHHOLZ = 2;
    public static final int ALGORITHM_WIN_PRECENTAGE = 3;
    public static final int ALGORITHM_PRECENTAGE_BUCHHOLZ = 4;

    public static final int ALGORITHM_SIMPLE_SWISS = 0;
    public static final int ALGORITHM_SWISS_REVERSED = 1;

    /* macierz punktow zdobytych przez graczy */
    @XStreamOmitField
    private int[][] matrix;

    public static int confPairingAlgorithm;
    public static int confPointsAlgorithm;
    public static boolean confPauseType;
    public static boolean confFirstRoundRandom;
    public static boolean confTopPairWeakest;
    public static boolean confTopSwitchPairs;
    public static boolean confTopDropPlayers;

    public Swiss(String name)
    {
        rounds = new ArrayList<Round>();
        players = new ArrayList<Player>();
        standings = new ArrayList<Player>();
        isFinished = false;
    }

    private void calculatePointsWinPrecentage() {

        if (rounds.isEmpty())
            return;

        // precision fix -> see ticket #1
        // we will calculate ranking on integers from 0-10000,
        // at the end divide it by 100.
        final int PRECISION = 10000;
        final int PRECISION_TO_100 = 100;

        // 1. calculating win precentage of each player
        int points1;
        int points2;

        for (Player pl : players)
            pl.setReserved2(0); // number of played rounds

        int numOfRounds = 0;

        for (Round r : rounds) {
            if (r.isTop())
                break;

            numOfRounds++;

            for (Pair pair : r.getPairs()) {

                points1 = pair.getPoints1();
                points2 = pair.getPoints2();

                int outcome;
                if (points1 == points2)
                    outcome = PRECISION / 2;
                else if (points1 == 0)
                    outcome = 0;
                else if (points2 == 0)
                    outcome = PRECISION;
                else
                    outcome = points1 * PRECISION / (points2 + points1);

                Player pl1 = pair.getPlayer1();
                Player pl2 = pair.getPlayer2();

                if (pl2 != Player.playerPause)
                    pl1.setReserved2(pl1.getReserved2() + 1);

                if (pl1 != Player.playerPause)
                    pl2.setReserved2(pl2.getReserved2() + 1);

                if (!confPauseType || (pl1 != Player.playerPause && pl2 != Player.playerPause)) {
                    pair.getPlayer1().addPoints(3, outcome);
                    pair.getPlayer2().addPoints(3, PRECISION - outcome);
                }
            }
        }

        if (numOfRounds == 0)
            return;

        // 1b. dividing win outcome by round count = win precentage
        for (Player pl : players) {
            if (confPauseType && pl.getReserved2() > 0)
                pl.setPoints(3, pl.getPoints(3) / pl.getReserved2());
            else
                pl.setPoints(3, pl.getPoints(3) / numOfRounds);
        }

        Player.playerPause.setPoints(3, Player.playerPause.getPoints(3) / numOfRounds);
        Player.playerMiss.resetPoints();

        // 2. sum of opponents' win precentage
        for (Round r : rounds) {
            if (r.isTop())
                break;

            for (Pair pair : r.getPairs()) {
                // sum of opponents points
                pair.getPlayer1().addPoints(1, pair.getPlayer2().getPoints(3));
                pair.getPlayer2().addPoints(1, pair.getPlayer1().getPoints(3));
            }
        }

        // 2b. bonus for playing less rounds
        if (confPauseType) {
            for (Player pl : players) {
                if (numOfRounds > pl.getReserved2())
                    pl.setPoints(1, pl.getPoints(1) * (2 * numOfRounds - pl.getReserved2()) / numOfRounds);
            }
        }

        // 3. sum of opponents' opponents' win precentage
        for (Round r : rounds) {
            if (r.isTop())
                break;

            for (Pair pair : r.getPairs()) {
                // sum of opponents opponents points
                pair.getPlayer1().addPoints(2, pair.getPlayer2().getPoints(1));
                pair.getPlayer2().addPoints(2, pair.getPlayer1().getPoints(1));
            }
        }

        // 3b. bonus for playing less rounds
        if (confPauseType) {
            for (Player pl : players) {
                if (numOfRounds > pl.getReserved2())
                    pl.setPoints(2, pl.getPoints(2) * (2 * numOfRounds - pl.getReserved2()) / numOfRounds);
            }
        }

        // 4. converting win percentage to range 0-100.
        for (Player pl : players) {
            // precision fix -> see ticket #1
            pl.setPoints(1, (int)Math.round((double)pl.getPoints(1) / PRECISION_TO_100));
            pl.setPoints(2, (int)Math.round((double)pl.getPoints(2) / PRECISION_TO_100));
            // 3rd set of points should be set to 0 in this algorithm
            pl.setPoints(3, 0);
        }
    }

    private void calculatePointsPrecentageBuchholtz() {

        if (rounds.isEmpty())
            return;

        // precision fix -> see ticket #1
        // we will calculate ranking on integers from 0-10000,
        // at the end divide it by 100.
        final int PRECISION = 10000;
        final int PRECISION_TO_100 = 100;

        for (Player pl : players)
            pl.setReserved2(0); // number of played rounds

        int numOfRounds = 0;

        for (Round r : rounds) {
            if (r.isTop())
                break;

            numOfRounds++;

            for (Pair pair : r.getPairs()) {

                Player pl1 = pair.getPlayer1();
                Player pl2 = pair.getPlayer2();

                if (pl2 != Player.playerPause)
                    pl1.setReserved2(pl1.getReserved2() + 1);

                if (pl1 != Player.playerPause)
                    pl2.setReserved2(pl2.getReserved2() + 1);

                if (!confPauseType || (pl1 != Player.playerPause && pl2 != Player.playerPause)) {
                    pl1.addPoints(3, pl2.getPoints(0));
                    pl2.addPoints(3, pl1.getPoints(0));
                }
            }
        }

        if (numOfRounds == 0)
            return;

        // 1b. dividing win outcome by round count = win precentage
        for (Player pl : players) {
            if (confPauseType && pl.getReserved2() > 0)
                pl.setPoints(1, pl.getPoints(3) * PRECISION / (2 * pl.getReserved2()));
            else
                pl.setPoints(1, pl.getPoints(3) * PRECISION / (2 * numOfRounds));
        }

        // 2. sum of opponents' opponents' win precentage
        for (Round r : rounds) {
            if (r.isTop())
                break;

            for (Pair pair : r.getPairs()) {
                // sum of opponents opponents points
                pair.getPlayer1().addPoints(2, pair.getPlayer2().getPoints(1));
                pair.getPlayer2().addPoints(2, pair.getPlayer1().getPoints(1));
            }
        }

        // 2b. bonus for playing less rounds
        for (Player pl : players) {
            if (confPauseType && pl.getReserved2() > 0)
                pl.setPoints(2, pl.getPoints(2) * (2 * numOfRounds - pl.getReserved2()) / numOfRounds);
        }

        // 4. converting win percentage to range 0-100.
        for (Player pl : players) {
            // precision fix -> see ticket #1
            pl.setPoints(1, (int)Math.round((double)pl.getPoints(1) / PRECISION_TO_100));
            pl.setPoints(2, (int)Math.round((double)pl.getPoints(2) / PRECISION_TO_100));
            // 3rd set of points should be set to 0 in this algorithm
            pl.setPoints(3, 0);
        }
    }

    private void calculatePoints()
    {
        // 0. Reseting all points
        for (Player pl : players)
            pl.resetPoints();

        Player.playerPause.resetPoints();
        Player.playerMiss.resetPoints();

        int numOfRounds = 0;

        // 1. calculating progress -> sum of our points
        for (Round r : rounds) {
            if (r.isTop())
                break;

            numOfRounds++;

            for (Pair pair : r.getPairs()) {
                pair.getPlayer1().addPoints(0, pair.getPoints1());
                pair.getPlayer2().addPoints(0, pair.getPoints2());
            }
        }

        if (confPointsAlgorithm == ALGORITHM_WIN_PRECENTAGE) {
            calculatePointsWinPrecentage();
            return;
        }

        if (confPointsAlgorithm == ALGORITHM_PRECENTAGE_BUCHHOLZ) {
            calculatePointsPrecentageBuchholtz();
            return;
        }

        int buchholtz;
        int berger;
        int numOfWins;

        switch (confPointsAlgorithm) {
            case ALGORITHM_NWINS_BERGER:
                buchholtz = 3;
                berger = 2;
                numOfWins = 1;
                break;
            case ALGORITHM_NWINS_BUCHHOLZ:
                buchholtz = 2;
                berger = 3;
                numOfWins = 1;
                break;
            case ALGORITHM_BUCHHOLTZ_BERGER:
            default:
                buchholtz = 1;
                berger = 2;
                numOfWins = 3;
        }

        /* if confPauseType option enabled, then we need reserved2
         * variable to remember if player meet pause */
        if (confPauseType) {
            for (Player pl : players) {
                pl.setReserved2(0);
            }
        }

        for (Round r : rounds) {
            if (r.isTop())
                break;

            for (Pair pair : r.getPairs()) {

                // Remember if player meet a pause
                boolean pausePlayer = false;
                if (confPauseType && pair.getPlayer2() == Player.playerPause || pair.getPlayer1() == Player.playerPause) {
                        pausePlayer = true;
                }

                // Buchholz -> sum of opponent's points
                if (!pausePlayer) {
                    pair.getPlayer1().setReserved2(pair.getPlayer1().getReserved2() + 1);
                    pair.getPlayer2().setReserved2(pair.getPlayer2().getReserved2() + 1);

                    // added only if opponent is not pause or confPauseType is off
                    pair.getPlayer1().addPoints(buchholtz, pair.getPlayer2().getPoints(0));
                    pair.getPlayer2().addPoints(buchholtz, pair.getPlayer1().getPoints(0));
                }

                // Berger -> our points * points of our opponent
                pair.getPlayer1().addPoints(berger, pair.getPoints1() * pair.getPlayer2().getPoints(0));
                pair.getPlayer2().addPoints(berger, pair.getPoints2() * pair.getPlayer1().getPoints(0));

                // Num of Wins -> how many times have we got more points than opponent
                pair.getPlayer1().addPoints(numOfWins, pair.getPoints1() > pair.getPoints2() ? 1 : 0);
                pair.getPlayer2().addPoints(numOfWins, pair.getPoints2() > pair.getPoints1() ? 1 : 0);
            }
        }

        /* if confPauseType option enabled, then Pause player
         * adds points to buchholtz tie-break equal to avarage points of opponents.
         * miss is treated as opponent with 0 points. */
        if (confPauseType && numOfRounds > 1) {
            for  (Player pl : players) {
                if (pl.getReserved2() < numOfRounds) {
                    pl.addPoints(buchholtz, (int)Math.round((double)pl.getPoints(buchholtz) * (numOfRounds - pl.getReserved2()) / numOfRounds));
                }
            }
        }

    }

    @SuppressWarnings("unchecked")
    private Round pairFirstRound(List<Player> players) {

        Round r = new Round();

        if (confFirstRoundRandom) {
            Collections.shuffle(players);
        } else {
            Collections.sort(players, new RankingComparator());
        }

        int playerSizeHalf = players.size() / 2;

        for (int i=0; i<playerSizeHalf; i++)
            r.addPair(players.get(i), players.get(i + playerSizeHalf));

        return r;
    }

    private Round pairSwiss(List<Player> players, int[][] matrix)
    throws SwissPairingNotPossible
    {
        if (rounds.isEmpty())
            return pairFirstRound(players);

        int isPlaying[] = new int[players.size()];
        for (int i=0; i<players.size(); i++)
            isPlaying[i] = NOT_PLAYED;

        int plid = 0;
        for (Player pl : players) {
            pl.setReserved2(plid++);
            // System.out.println(plid + ". " + pl);
        }

        // nice and fast paring algorithm.
        Round r = new Round();
        int i = 0;
        int j = 1;

        while (i < players.size()) {

            if (isPlaying[i] != NOT_PLAYED) {
                i++; continue;
            }

            while (j < players.size()) {
                if (i == j) {
                    j++; continue;
                }

                if (isPlaying[j] != NOT_PLAYED) {
                    j++; continue;
                }

                Player plA = players.get(i);
                Player plB = players.get(j);

                if (matrix[plA.getReserved1()][plB.getReserved1()] != NOT_PLAYED) {
                    j++; continue;
                }

                isPlaying[i] = j;
                isPlaying[j] = i;
                r.addPair(plA, plB);
                //System.out.println("      added pair " + plA + " - " + plB);
                break;
            }

            if (isPlaying[i] == NOT_PLAYED) {

                if (i == 0)
                    throw new SwissPairingNotPossible();

                Pair p = r.removeLastPair();
                i = p.getPlayer1().getReserved2();
                j = p.getPlayer2().getReserved2();

                //System.out.println("      removed " + p.getPlayer1().getName() + " - " + p.getPlayer2().getName());
                isPlaying[i] = NOT_PLAYED;
                isPlaying[j] = NOT_PLAYED;

                j = j + 1;

            } else {
                i = i + 1;
                j = 0;
            }
        }

        return r;
    }

    private void updateMatrix(List<Player> players, List<Round> rounds) {


        // tworzenie pustej tabeli results.
        // W przyszłości przeniosę to do innej funkcji i będzie
        // tworzył aktualną tabelę na bazie listy rounds
        if (matrix == null || matrix.length != players.size()+1) {
            matrix = new int[players.size()+1][players.size()+1];
        }

        for (int j=0; j<players.size()+1; j++)
        for (int i=0; i<players.size()+1; i++)
            matrix[i][j] = NOT_PLAYED;

        int id = 0;
        for (Player pl : players)
            pl.setReserved1(id++);
        Player.playerPause.setReserved1(id);

        for (Round r : rounds)
        for (Pair p : r.getPairs()) {

            if (p.getPlayer1() == Player.playerMiss ||
                p.getPlayer2() == Player.playerMiss)
                continue;

            matrix[p.getPlayer1().getReserved1()][p.getPlayer2().getReserved1()] = p.getPoints1();
            matrix[p.getPlayer2().getReserved1()][p.getPlayer1().getReserved1()] = p.getPoints2();
        }
    }

    @Override
    public Round startEmptyRound()
    throws SwissTournamentFinished, SwissEmptyPlayerList, SwissResultsNotFilled,
    SwissIllegalPairs, SwissPairingNotPossible, SwissPairingTopNotPossible
    {
        if (isFinished())
            throw new SwissTournamentFinished();

        List<Player> playingPlayers = new ArrayList<Player>();
        for (Player pl : players) {
            if (!pl.isLeaved()) {
                playingPlayers.add(pl);
            }
        }

        if (playingPlayers.isEmpty())
            throw new SwissEmptyPlayerList();

        for (Round r : rounds)
            validateRound(r);

        int initTop = findInitialTop();
        if (initTop != 0) {
            return startTopRound(initTop, currentRound().getTop() / 2);
        }

        Round r = new Round();
        rounds.add(r);
        r.setNumber(rounds.size());

        return r;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Round startNewRound()
    throws SwissTournamentFinished, SwissEmptyPlayerList, SwissResultsNotFilled,
    SwissIllegalPairs, SwissPairingNotPossible, SwissPairingTopNotPossible
    {
        if (isFinished())
            throw new SwissTournamentFinished();

        List<Player> playingPlayers = new ArrayList<Player>();
        List<Player> leavedPlayers = new ArrayList<Player>();
        for (Player pl : players) {
            if (!pl.isLeaved()) {
                playingPlayers.add(pl);
            } else {
                leavedPlayers.add(pl);
            }
        }

        if (playingPlayers.isEmpty())
            throw new SwissEmptyPlayerList();

        for (Round r : rounds)
            validateRound(r);

        int initTop = findInitialTop();
        if (initTop != 0) {
            return startTopRound(initTop, currentRound().getTop() / 2);
        }

        if (playingPlayers.size() % 2 == 1)
            playingPlayers.add(Player.playerPause);

        updateMatrix(players, rounds);

        calculatePoints();

        Collections.shuffle(playingPlayers);

        if (confPairingAlgorithm == ALGORITHM_SIMPLE_SWISS)
            Collections.sort(playingPlayers, new RandomizedTieBreakComparator());
        else if (confPairingAlgorithm == ALGORITHM_SWISS_REVERSED) {
            if (rounds.size() % 2 == 0)
                Collections.sort(playingPlayers);
            else
                Collections.sort(playingPlayers, new ReversedTieBreakComparator());
        }

        Round r = pairSwiss(playingPlayers, matrix);

        for (Player pl : leavedPlayers)
            r.addPair(new Pair(pl, Player.playerMiss));

        rounds.add(r);
        r.setNumber(rounds.size());

        return r;
    }

    @Override
    public void deleteLastRound()
    throws SwissNoRounds, SwissTournamentFinished
    {
        if (isFinished())
            throw new SwissTournamentFinished();

        if (rounds.isEmpty())
            throw new SwissNoRounds();

        rounds.remove(currentRound());

        // optional step - recalculate points
        calculatePoints();
    }


    @SuppressWarnings("unchecked")
    @Override
    public void finishTournament() throws SwissResultsNotFilled, SwissIllegalPairs,
    SwissEmptyPlayerList, SwissTournamentFinished, SwissNoRounds, SwissTopRoundMissing, SwissTopRoundMissing {

        if (isFinished())
            throw new SwissTournamentFinished();

        if (players.isEmpty())
            throw new SwissEmptyPlayerList();

        if (rounds.isEmpty())
            throw new SwissNoRounds();

        Round lastRound = currentRound();
        if (lastRound.isTop() && lastRound.getTop() != 2) {
            throw new SwissTopRoundMissing();
        }

        for (Round r : rounds)
            validateRound(r);


        // generating standings
        calculatePoints();

        standings.clear();
        for (Player p : players)
            standings.add(p);

        Collections.shuffle(standings);
        Collections.sort(standings);

        fixTopStandings();
        
        this.calculateRanking();

        isFinished = true;

    }

    @SuppressWarnings("unchecked")
    public void fixTopStandings() throws SwissResultsNotFilled
    {
        int numOfRound = rounds.size() - 1;
        int i = 0;
        Round currentRound = rounds.get(numOfRound);

        // changing standings according to top-rounds results for last round
        if (currentRound.isTop()) {
            int numOfPlayers = currentRound.getPairs().size() * 2;
            for (i = 0; i<numOfPlayers && i<standings.size(); i+=2) {
            
                Pair pair = currentRound.getPair(i/2);
                Player player1 = pair.getWinner();
                if (player1 == null)
                    throw new SwissResultsNotFilled();

                standings.remove(player1);
                if (player1 != Player.playerMiss && player1 != Player.playerPause)
                    standings.add(i, player1);

                Player player2 = pair.getLosser();
                if (player2 == null)
                    throw new SwissResultsNotFilled();

                standings.remove(player2);
                if (player2 != Player.playerMiss && player2 != Player.playerPause)
                    standings.add(i+1, player2);
            }
        }
        
        // for all other top rounds
        List<Player> topPlayers = new ArrayList<Player>();
        while (numOfRound > 0 && (currentRound = rounds.get(--numOfRound)) != null && currentRound.isTop()) {

            if (i >= currentRound.getPairs().size() * 2) {
                continue;
            }
            
            topPlayers.clear();
            for (Pair pair : currentRound.getPairs()) {
                
                Player player1 = pair.getPlayer1();
                if (standings.indexOf(player1) < i)
                    player1 = pair.getPlayer2();
                topPlayers.add(player1);
            }
            
            // determine the order of top players by swiss result
            Collections.sort(topPlayers);
            for (Player p : topPlayers) {
                standings.remove(p);
                standings.add(i++, p);
            }
        }
    }

    @Override
    public Round startTopRound(int initTop) throws SwissTournamentFinished, SwissEmptyPlayerList,
    SwissResultsNotFilled, SwissIllegalPairs, SwissPairingTopNotPossible {

        if (players.isEmpty())
            throw new SwissEmptyPlayerList();

        if (rounds.size() > 0) {
            Round lastRound = currentRound();
            if (lastRound.isTop())
                throw new SwissPairingTopNotPossible();
            else if (! lastRound.allResultsFilled())
                throw new SwissResultsNotFilled();
        }

        return startTopRound(initTop, initTop);
    }
    
    @SuppressWarnings("unchecked")
    private Round startTopRoundFirstRound(int initTop, int top) throws SwissTournamentFinished, SwissEmptyPlayerList,
    SwissResultsNotFilled, SwissIllegalPairs, SwissPairingTopNotPossible {
        Round r = new Round();
        r.setTop(top);

        // paring basis on the swiss results (or random)
        List<Player> playersTop = new ArrayList<Player>();

        // generating standings
        calculatePoints();

        // sorting players in the temporary list, because
        // we don't want to change order in the list players.
        List<Player> playersTmp = new ArrayList<Player>(players);
        Collections.sort(playersTmp);

        playersTop.clear();
        for (Player p : playersTmp) {
            playersTop.add(p);
            if (playersTop.size() >= top)
                break;
        }

        Collections.shuffle(playersTop);
        Collections.sort(playersTop);

        // adding players in specified order
        int high = 0;
        int low = top - 1;
        int numOfPlayers = playersTop.size();
        while (high < low) {

            if (numOfPlayers <= high)
                break;

            Player player1 = playersTop.get(high);
            Player player2;

            if (numOfPlayers <= low)
                player2 = Player.playerMiss;
            else
                player2 = playersTop.get(low);

            r.addPair(player1, player2);

            high++;
            low--;
        }

        return r;
    }

    @SuppressWarnings("unchecked")
    private Round startTopRoundFirstRoundOrdered(int initTop, int top) throws SwissTournamentFinished, SwissEmptyPlayerList,
    SwissResultsNotFilled, SwissIllegalPairs, SwissPairingTopNotPossible {
        Round r = new Round();
        r.setTop(top);

        // paring basis on the swiss results (or random)
        List<Player> playersTop = new ArrayList<Player>();

        // generating standings
        calculatePoints();

        // sorting players in the temporary list, because
        // we don't want to change order in the list players.
        List<Player> playersTmp = new ArrayList<Player>(players);
        Collections.sort(playersTmp);

        playersTop.clear();
        for (Player p : playersTmp) {
            playersTop.add(p);
            if (playersTop.size() >= top)
                break;
        }

        Collections.shuffle(playersTop);
        Collections.sort(playersTop);

        // adding players in specified order
        int high = 0;
        int numOfPlayers = playersTop.size();
        while (high < top - 1) {

            if (numOfPlayers <= high)
                break;

            Player player1 = playersTop.get(high++);

            Player player2;
            if (numOfPlayers <= high)
                player2 = Player.playerMiss;
            else
                player2 = playersTop.get(high++);

            r.addPair(player1, player2);
        }

        return r;
    }

    private Round startTopRoundNextRound(int initTop, int top, Round lastRound) throws SwissTournamentFinished, SwissEmptyPlayerList,
    SwissResultsNotFilled, SwissIllegalPairs, SwissPairingTopNotPossible {
        Round r = new Round();
        r.setTop(top);

        // paring on basis the previous round
        List<Pair> lastPairs = lastRound.getPairs();
        int numOfPairs = lastPairs.size();

        initTop = numOfPairs * 2;
        int globalHigh = 0;
        while (globalHigh < initTop) {

            // pairing the winners first
            int high = globalHigh;
            int low = globalHigh + top - 1;
            while (high < low) {
                Player winnerHigh;
                Player winnerLow;

                if (numOfPairs <= high)
                    break;
                else {
                    Pair pairHigh = lastPairs.get(high);
                    winnerHigh = pairHigh.getWinner();
                    if (winnerHigh == null)
                        throw new SwissResultsNotFilled();
                }

                if (numOfPairs <= low)
                    winnerLow = Player.playerMiss;
                else {
                    Pair pairLow = lastPairs.get(low);
                    winnerLow = pairLow.getWinner();
                    if (winnerLow == null)
                        throw new SwissResultsNotFilled();
                }

                r.addPair(new Pair(winnerHigh, winnerLow));
                high++;
                low--;
            }

            if (!confTopDropPlayers) {
                high = globalHigh;
                low = globalHigh + top - 1;
                while (high < low) {
                    Player losserHigh;
                    Player losserLow;

                    if (numOfPairs <= high)
                        break;
                    else {
                        Pair pairHigh = lastPairs.get(high);
                        losserHigh = pairHigh.getLosser();
                        if (losserHigh == null)
                            throw new SwissResultsNotFilled();
                    }

                    if (numOfPairs <= low)
                        losserLow = Player.playerMiss;
                    else {
                        Pair pairLow = lastPairs.get(low);
                        losserLow = pairLow.getLosser();
                        if (losserLow == null)
                            throw new SwissResultsNotFilled();
                    }

                    r.addPair(new Pair(losserHigh, losserLow));
                    high++;
                    low--;
                }
            }

            globalHigh += top;
        }

        return r;
    }

    private Round startTopRoundNextRoundNormal(int initTop, int top, Round lastRound) throws SwissTournamentFinished, SwissEmptyPlayerList,
    SwissResultsNotFilled, SwissIllegalPairs, SwissPairingTopNotPossible {
        Round r = new Round();
        r.setTop(top);

        // paring on basis the previous round
        List<Pair> lastPairs = lastRound.getPairs();
        int numOfPairs = lastPairs.size();

        initTop = numOfPairs * 2;
        int globalHigh = 0;
        while (globalHigh < initTop) {

            // pairing the winners first
            int high = globalHigh;
            int low = globalHigh + top - 1;
            while (high < low) {
                Player winnerHigh;
                Player winnerLow;

                if (numOfPairs <= high)
                    break;
                else {
                    Pair pairHigh = lastPairs.get(high);
                    winnerHigh = pairHigh.getWinner();
                    if (winnerHigh == null)
                        throw new SwissResultsNotFilled();
                }
                high++;

                if (numOfPairs <= high)
                    winnerLow = Player.playerMiss;
                else {
                    Pair pairLow = lastPairs.get(high);
                    winnerLow = pairLow.getWinner();
                    if (winnerLow == null)
                        throw new SwissResultsNotFilled();
                }

                r.addPair(new Pair(winnerHigh, winnerLow));
                high++;
            }

            if (!confTopDropPlayers) {
                high = globalHigh;
                low = globalHigh + top - 1;
                while (high < low) {
                    Player losserHigh;
                    Player losserLow;

                    if (numOfPairs <= high)
                        break;
                    else {
                        Pair pairHigh = lastPairs.get(high);
                        losserHigh = pairHigh.getLosser();
                        if (losserHigh == null)
                            throw new SwissResultsNotFilled();
                    }
                    high++;

                    if (numOfPairs <= high)
                        losserLow = Player.playerMiss;
                    else {
                        Pair pairLow = lastPairs.get(high);
                        losserLow = pairLow.getLosser();
                        if (losserLow == null)
                            throw new SwissResultsNotFilled();
                    }

                    r.addPair(new Pair(losserHigh, losserLow));
                    high++;
                }
            }

            globalHigh += top;
        }

        return r;
    }

    @SuppressWarnings("unchecked")
    private Round startTopRound(int initTop, int top) throws SwissTournamentFinished, SwissEmptyPlayerList,
    SwissResultsNotFilled, SwissIllegalPairs, SwissPairingTopNotPossible {

        if (top < 2 || players.size() < top)
            throw new SwissPairingTopNotPossible();

        Round r;
        Round lastRound;
        if (!rounds.isEmpty() && (lastRound = currentRound()).isTop()) {
            if (confTopSwitchPairs)
                r = startTopRoundNextRound(initTop, top, lastRound);
            else
                r = startTopRoundNextRoundNormal(initTop, top, lastRound);
        } else {
            if (confTopPairWeakest)
                r = startTopRoundFirstRound(initTop, top);
            else
                r = startTopRoundFirstRoundOrdered(initTop, top);
        }

        // adding round to tournament
        rounds.add(r);
        r.setNumber(rounds.size());

        // returning new round
        return r;
    }



}
