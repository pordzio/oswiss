/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.katowice.poldrag.swiss.core;

import java.util.List;
import pl.katowice.poldrag.swiss.exceptions.*;

/**
 *
 * @author kkopec
 */
public interface Tournament {

    public boolean addPlayer(Player player)
            throws SwissPlayerNameCollision, SwissIllegalPlayerName, SwissTournamentFinished;

    public void delPlayer(int ind)
            throws SwissTournamentFinished;

    public Round startNewRound()
            throws SwissTournamentFinished, SwissEmptyPlayerList, SwissResultsNotFilled,
            SwissIllegalPairs, SwissPairingNotPossible, SwissPairingTopNotPossible;

    public Round startEmptyRound()
            throws SwissTournamentFinished, SwissEmptyPlayerList, SwissResultsNotFilled,
            SwissIllegalPairs, SwissPairingNotPossible, SwissPairingTopNotPossible;

    public Round startTopRound(int initTop)
            throws SwissTournamentFinished, SwissEmptyPlayerList, SwissResultsNotFilled,
            SwissIllegalPairs, SwissPairingTopNotPossible;

    public void deleteLastRound()
            throws SwissNoRounds, SwissTournamentFinished;

    public Round currentRound();

    public int getRoundsCount();

    public Round getRound(int i);

    public List<Player> getPlayerList();

    public List<Player> getStandings();

    public void finishTournament()
            throws SwissResultsNotFilled, SwissIllegalPairs, SwissEmptyPlayerList,
            SwissTournamentFinished, SwissNoRounds, SwissTopRoundMissing;

    public void resumeTournament()
            throws SwissTournamentFinished;

    public List<Pair> getPlayerPairs(Player pl);

    public boolean isFinished();
    
    public int findInitialTop();
}
